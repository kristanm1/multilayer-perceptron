#include "utils.h"


void set_fun_tab(double start, double end, unsigned int size) {
    fun_tab_low = start;
    fun_tab_high = end;
    fun_tab_size = size;
}

double lin_int(double x1, double y1, double x2, double y2, double x) {
    double k = (y2 - y1) / (x2 - x1);
    double n = ((y1 - x1*k) + (y2 - x2*k)) / 2.0;
    return k*x + n;
}

long q_lin_int(long q_x1, long q_y1, long q_x2, long q_y2, long q_x) {
    long q_k = q_div(q_sub(q_y2, q_y1), q_sub(q_x2, q_x1));
    long q_n = q_add(q_sub(q_y1, q_mul(q_x1, q_k)), q_sub(q_y2, q_mul(q_x2, q_k))) >> 1;
    return q_add(q_mul(q_k, q_x), q_n);
}

/**********************************************************************************************************************************/

unsigned int find_bis(double x, double start, double end, double step, unsigned int size) {
    if(x < start) {
        return 0;
    } else if(x >= end) {
        return size - 1;
    }
    unsigned int a = 0, c = size - 1;
    while(a + 1 < c) {
        unsigned int b = a + ((c - a) >> 1);
        //printf(" bis: a: %u b: %u c: %u\n", a, b, c);
        //printf("%g %g %g\n\n", start + a*step, start + b*step, start + c*step);
        if(x < start + b*step) {
            c = b;
        } else {
            a = b;
        }
    }
    return a;
}

unsigned int q_find_bis(long x, long *xs, unsigned int size) {
    if(x < xs[0]) {
        return 0;
    } else if(x >= xs[size - 1]) {
        return size - 1;
    }
    unsigned int a = 0, c = size - 1;
    while(a + 1 < c) {
        unsigned int b = a + ((c - a) >> 1);
        //printf("qbis: a: %u b: %u c: %u\n", a, b, c);
        //printf("%g %g\n\n", fixed_to_double(xi), fixed_to_double(tmp));
        if(x < xs[b]) {
            c = b;
        } else {
            a = b;
        }
    }
    return a;
}

/**********************************************************************************************************************************/

fun_tab *alloc_fun_tab(double start, double end, unsigned int size, double (*fun)(double)) {
    fun_tab *tab = (fun_tab*) malloc(sizeof(fun_tab));
    tab->size = size;
    tab->start = start;
    tab->end = end;
    tab->step = (end - start) / (size - 1);
    tab->y = (double*) malloc(sizeof(double)*size);
    unsigned int i;
    for(i = 0; i < size; i++) {
        tab->y[i] = fun(start + i*tab->step);
    }
    return tab;
}

fun_tab *alloc_load_fun_tab(char *filename) {
    FILE *f = fopen(filename, "rb");
    fun_tab *tab = (fun_tab*) malloc(sizeof(fun_tab));
    int r;
    r = fscanf(f, "%u\n", &tab->size);
    tab->y = (double*) malloc(sizeof(double)*tab->size);
    r = fread(&tab->start, sizeof(double), 3, f);
    r = fread(tab->y, sizeof(double), tab->size, f);
    fclose(f);
    return tab;
}

double f_fun_tab(double x, fun_tab *tab) {
    unsigned int i = find_bis(x, tab->start, tab->end, tab->step, tab->size);
    if(i == tab->size - 1) {
        return tab->y[i];
    }
    double x1 = tab->start + i*tab->step;
    double x2 = x1 + tab->step;
    return lin_int(x1, tab->y[i], x2, tab->y[i + 1], x);
}

void save_fun_tab(char *filename, fun_tab *tab) {
    FILE *f = fopen(filename, "wb");
    fprintf(f, "%u\n", tab->size);
    fwrite(&tab->start, sizeof(double), 3, f);
    fwrite(tab->y, sizeof(double), tab->size, f);
    fclose(f);
}

void gen_save_fun_tab(double start, double end, unsigned int size, char *filename, double (*fun)(double)) {
    fun_tab *tab = alloc_fun_tab(start, end, size, fun);
    save_fun_tab(filename, tab);
    free_fun_tab(tab);
}

void free_fun_tab(fun_tab *tab) {
    free(tab->y);
    free(tab);
}

/**********************************************************************************************************************************/

q_fun_tab *alloc_q_fun_tab(double start, double end, unsigned int size, double (*fun)(double)) {
    q_fun_tab *q_tab = (q_fun_tab*) malloc(sizeof(q_fun_tab));
    q_tab->size = size;
    q_tab->start = double_to_fixed(start);
    q_tab->end = double_to_fixed(end);
    double step = (end - start) / (size - 1);
    q_tab->step = double_to_fixed(step);
    if(q_tab->step == 0L) {
        printf("WARNING: q_fun_tab->step is 0 (zero)!\n");
    }
    q_tab->x = (long*) malloc(sizeof(long)*2*size);
    q_tab->y = q_tab->x + size;
    unsigned int i;
    for(i = 0; i < size; i++) {
        double x = start + i*step;
        q_tab->x[i] = double_to_fixed(x);
        q_tab->y[i] = double_to_fixed(fun(x));
    }
    return q_tab;
}

q_fun_tab *alloc_load_q_fun_tab(char *filename) {
    FILE *f = fopen(filename, "rb");
    q_fun_tab *q_tab = (q_fun_tab*) malloc(sizeof(q_fun_tab));
    int r;
    r = fscanf(f, "%u\n", &q_tab->size);
    q_tab->x = (long*) malloc(sizeof(long)*2*q_tab->size);
    q_tab->y = q_tab->x + q_tab->size;
    r = fread(q_tab->x, sizeof(long), q_tab->size, f);
    r = fread(q_tab->y, sizeof(long), q_tab->size, f);
    fclose(f);
    return q_tab;
}

long f_q_fun_tab(long q_x, q_fun_tab *q_tab) {
    long i = q_find_bis(q_x, q_tab->x, q_tab->size);
    if(i == q_tab->size - 1) {
        return q_tab->y[i];
    }
    return q_lin_int(q_tab->x[i], q_tab->y[i], q_tab->x[i + 1], q_tab->y[i + 1], q_x);
}

void save_q_fun_tab(char *filename, q_fun_tab *q_tab) {
    FILE *f = fopen(filename, "wb");
    fprintf(f, "%u\n", q_tab->size);
    fwrite(&q_tab->start, sizeof(long), 3, f);
    fwrite(q_tab->x, sizeof(long), q_tab->size, f);
    fwrite(q_tab->y, sizeof(long), q_tab->size, f);
    fclose(f);
}

void gen_save_q_fun_tab(long start, long end, unsigned int size, char *filename, double (*fun)(double)) {
    q_fun_tab *q_tab = alloc_q_fun_tab(start, end, size, fun);
    save_q_fun_tab(filename, q_tab);
    free_q_fun_tab(q_tab);
}

void free_q_fun_tab(q_fun_tab *q_tab) {
    free(q_tab->x);
    free(q_tab);
}

/**********************************************************************************************************************************/

unsigned int my_argmax(int size, double *array) {
    double max = array[0];
    int i, argmax = 0;
    for(i = 1; i < size; i++) {
        if(max < array[i]) {
            max = array[i];
            argmax = i;
        }
    }
    return argmax;
}

unsigned int my_argmin(int size, double *array) {
    double min = array[0];
    int i, argmin = 0;
    for(i = 1; i < size; i++) {
        if(array[i] < min) {
            min = array[i];
            argmin = i;
        }
    }
    return argmin;
}

double my_abs(double x) {
    if(x < 0) {
        return -x;
    } else {
        return x;
    }
}

long my_round(double x) {
    return (long) (x + 0.5);
}

int my_is_nan(double x) {
    unsigned long xi = *((unsigned long*) &x);
    return (xi & 0xFFFFFFFFFFFFF) > 0 && ((xi >> 52) & 0x7FF) == 0x7FF;
}

/**********************************************************************************************************************************/

int my_q_argmax(int size, long *array) {
    long max = array[0];
    int i, argmax = 0;
    for(i = 1; i < size; i++) {
        if(max < array[i]) {
            max = array[i];
            argmax = i;
        }
    }
    return argmax;
}

/**********************************************************************************************************************************/

double array_mean(int size, double *array) {
    double avg = 0.0;
    int i;
    for(i = 0; i < size; i++) {
        avg += array[i];
    }
    return avg/size;
}

double array_sd(int size, double avg, double *array) {
    double std = 0.0;
    int i;
    for(i = 0; i < size; i++) {
        double tmp = array[i] - avg;
        std += tmp*tmp;
    }
    return sqrt(std/size);
}

/**********************************************************************************************************************************/

void print_bits(unsigned long x) {
    int n = 0;
    unsigned long t = x;
    while(t > 0) {
        n++;
        t >>= 1;
    }
    int nb = n/4 - (n%4 == 0);
    char bits[n + nb + 1];
    bits[0] = '\0';
    bits[n + nb] = '\0';
    t = x;
    int i, p = n + nb - 1;
    for(i = 0; i < n; i++) {
        if(i > 0 && (i%4 == 0)) {
            bits[p--] = (i%8 == 0) ? '|' : ' ';
        }
        bits[p--] = t%2 + '0';
        t >>= 1;
    }
    printf("%s\n", bits);
}

/**********************************************************************************************************************************/

unsigned long ulong_ma_mul(unsigned long a, unsigned long b) {
    if(a == 0L || b == 0L) {
        return 0L;
    } else if(a == 1L) {
        return b;
    } else if(b == 1L) {
        return a;
    }
    int k1 = 0, k2 = 0, k12;
    unsigned long x1, x2, x12, temp;
    temp = a;
    while((temp >>= 1) > 0L) {
        k1++;
    }
    temp = b;
    while((temp >>= 1) > 0L) {
        k2++;
    }
    k12 = k1 + k2;
    x1 = a << (64 - k1);
    x2 = b << (64 - k2);
    x12 = x1 + x2;
    if(x12 < x1) {
        k12++;
    }
    x12 >>= 1;
    x12 |= (1L << 63);
    return x12 >> (64 - k12 - 1);
}

long long_ma_mul(long a, long b) {
    char sa = 1, sb = 1;
    if(a < 0) {
        a = -a;
        sa = -1;
    }
    if(b < 0) {
        b = -b;
        sb = -1;
    }
    long c = ulong_ma_mul(a, b);
    return (sa*sb == -1) ? -c : c;
}

unsigned long ulong_ilm(unsigned long n1, unsigned long n2) {
    int k1, k2, i;
    unsigned long p, temp;
    // approximate n1*n2
    k1 = 0; temp = n1;
    while((temp >>= 1) > 0L) {
        k1++;
    }
    k2 = 0; temp = n2;
    while((temp >>= 1) > 0L) {
        k2++;
    }
    n1 ^= (1L << k1);
    n2 ^= (1L << k2);
    p = (1L << (k1 + k2)) + (n1 << k2) + (n2 << k1);

    // 1st correction iteration
    if(n1 > 0L && n2 > 0L) {
        k1 = 0; temp = n1;
        while((temp >>= 1) > 0L) {
            k1++;
        }
        k2 = 0; temp = n2;
        while((temp >>= 1) > 0L) {
            k2++;
        }
        n1 ^= (1L << k1);
        n2 ^= (1L << k2);
        p += (1L << (k1 + k2)) + (n1 << k2) + (n2 << k1);
    }

    return p;
}

long long_ilm(long n1, long n2) {
    char s1 = 1, s2 = 1;
    if(n1 < 0) {
        n1 = -n1;
        s1 = -1;
    }
    if(n2 < 0) {
        n2 = -n2;
        s2 = -1;
    }
    long p = ulong_ilm(n1, n2);
    return (s1*s2 == -1) ? -p : p;
}

/**********************************************************************************************************************************/



/**********************************************************************************************************************************/
