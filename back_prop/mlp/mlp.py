import numpy as np
import matplotlib.pyplot as plt


def sigma(x):
	return 1/(1 + np.exp(-x))


def d_sigma(x):
	y = sigma(x)
	return y * (1 - y)


def forward(ws, bs, x):
	zs, acs = [], [x]
	for w, b in zip(ws, bs):
		zs.append(np.dot(w, acs[-1]) + b)
		acs.append(sigma(zs[-1]))
	return zs, acs


def backprop(ws, bs, x, y):
	zs, acs = forward(ws, bs, x)
	dbs = [(acs[-1] - y) * d_sigma(zs[-1])]
	dws = [np.outer(dbs[0], acs[-2])]
	for i in range(len(ws) - 2, -1, -1):
		dbs = [d_sigma(zs[i]) * np.dot(np.transpose(ws[i+1]), dbs[0])] + dbs
		dws = [np.outer(dbs[0], acs[i])] + dws
	return dws, dbs


def grad_batch(ws, bs, xs, ys):
	dws, dbs = [np.zeros(w.shape) for w in ws], [np.zeros(b.shape) for b in bs]
	for x, y in zip(xs, ys):
		dws_t, dbs_t = backprop(ws, bs, x, y)
		for i in range(len(ws)):
			dws[i] += dws_t[i]
			dbs[i] += dbs_t[i]
	return dws, dbs

def grad_desc(ws, bs, dws, dbs, eta):
	for i in range(len(ws)):
		ws[i] -= eta*dws[i]
		bs[i] -= eta*dbs[i]


def train(ws, bs, xs_t, ys_t, xs_v, ys_v, epochs, batch_size, learning_rate):
	for epoch in range(epochs):
		i = 0
		while i < len(xs_t):
			j = i + batch_size
			if j >= len(xs_t):
				j = len(xs_t)
			dws, dbs = grad_batch(ws, bs, xs_t[i:j], ys_t[i:j])
			grad_desc(ws, bs, dws, dbs, learning_rate)
			i += batch_size
		cost, tpr = evaluate(ws, bs, xs_v, ys_v)
		print('{:4d}| cost: {:.10f} tpr: {:.4f}'.format(epoch, cost, tpr))


def cost_x(ws, bs, x, y):
	_, acs = forward(ws, bs, x)
	return np.sum((y - acs[-1])**2)/2.0, 1 if np.argmax(y) == np.argmax(acs[-1]) else 0


def evaluate(ws, bs, xs, ys):
	cost, tpr = 0.0, 0
	for x, y in zip(xs, ys):
		cost_t, tp_t = cost_x(ws, bs, x, y)
		cost += cost_t
		tpr += tp_t
	return cost/len(xs), tpr/len(xs)


def init_nrand(layers, avg, std):
	ws, bs = [], []
	for i in range(1, len(layers)):
		ws.append(np.random.normal(avg, std, (layers[i], layers[i - 1])))
		bs.append(np.random.normal(avg, std, layers[i]))
	return ws, bs


def load_mlp(filename):
	with open(filename, 'rb') as f:
		size = int(f.readline())
		layers = [int.from_bytes(f.read(4), 'little') for _ in range(size)]
		ws = [np.frombuffer(f.read(8*layers[i]*layers[i-1]), dtype=np.float64).reshape(layers[i], layers[i-1]).copy() for i in range(1, size)]
		bs = [np.frombuffer(f.read(8*layers[i]), dtype=np.float64).copy() for i in range(1, size)]
		return ws, bs
	

def save_mlp(ws, bs, filename):
	with open(filename, 'wb') as f:
		layers = [ws[0].shape[1]] + [b.size for b in bs]
		f.write(bytes(str(len(ws) + 1) + '\n', 'ascii'))
		for i in range(len(layers)):
			f.write(layers[i].to_bytes(4, 'little'))
		for i in range(len(ws)):
			f.write(ws[i].reshape(layers[i]*layers[i+1]).tobytes())
		for i in range(len(bs)):
			f.write(bs[i].tobytes())


def test_init_mlp():
	ws = [np.array([[0.6, -0.7, 0.3], [-0.8, -0.7, -0.2], [0.4, -0.6, 0.2], [-0.4, -0.6, 0.7]]),
		np.array([[-0.1, 0.3, -0.2, -0.3], [-0.7, 0.7, -0.9, 0.9]])]
	bs = [np.array([-0.5, 1.0, -0.2, 0.7]), np.array([0.1, 0.8])]
	return ws, bs


def test_init_dataset():
	xs = [np.array([1.0, 2.0, 3.0]), np.array([3.0, 2.0, 1.0])]
	ys = [np.array([0.9, 0.1]), np.array([0.1, 0.9])]
	return xs, ys

def simple_dataset():
	xs = [np.array([0, 1, 1, 0]), np.array([1, 0, 0, 1]), np.array([0, 1, 0, 1]),
		np.array([1, 0, 1, 0]), np.array([1, 1, 0, 0]), np.array([0, 0, 1, 1])]
	ys = [np.array([0.9, 0.1, 0.1]), np.array([0.9, 0.1, 0.1]), np.array([0.1, 0.9, 0.1]),
		np.array([0.1, 0.9, 0.1]), np.array([0.1, 0.1, 0.9]), np.array([0.1, 0.1, 0.9])]
	return xs, ys

def load_MNIST(images, labels):
	with open(images, 'rb') as f_i, open(labels, 'rb') as f_l:
		_, _ = f_i.read(4), f_l.read(4)
		size, _ = int.from_bytes(f_i.read(4), 'big'), int.from_bytes(f_l.read(4), 'big')
		nx_i = int.from_bytes(f_i.read(4), 'big')
		ny_i = int.from_bytes(f_i.read(4), 'big')
		xs, ys = [], []
		for _ in range(size):
			x = np.frombuffer(f_i.read(nx_i*ny_i), dtype=np.uint8)
			xs.append(x.astype(np.float64))
			y = np.ones(10)*0.1
			y[int.from_bytes(f_l.read(1), 'big')] = 0.9
			ys.append(y)
		return xs, ys


def test_MNIST_avg_std():
	ws, bs = load_mlp('init.mlp')
	xs_t, _ = load_MNIST('MNIST_unzipped/train-images.idx3-ubyte', 'MNIST_unzipped/train-labels.idx1-ubyte')
	num_neurons = sum([w.shape[0] for w in ws])
	activations = np.zeros(len(xs_t)*num_neurons)
	for i in range(len(xs_t)):
		zs, _ = forward(ws, bs, xs_t[i])
		activations[i*num_neurons:(i+1)*num_neurons] = np.hstack(zs)
	print('avg: {:.9g}'.format(np.mean(activations)))
	print('std: {:.9g}'.format(np.std(activations)))


def train_MNIST(epochs=13, batch_size=100, learning_rate=0.001):
	ws, bs = load_mlp('init.mlp')
	xs_t, ys_t = load_MNIST('MNIST_unzipped/train-images.idx3-ubyte', 'MNIST_unzipped/train-labels.idx1-ubyte')
	#size = 1000
	#xs_t, ys_t = xs_t[:size], ys_t[:size]
	xs_v, ys_v = load_MNIST('MNIST_unzipped/t10k-images.idx3-ubyte', 'MNIST_unzipped/t10k-labels.idx1-ubyte')

	train(ws, bs, xs_t, ys_t, xs_v, ys_v, epochs, batch_size, learning_rate)




#ws, bs = test_init_mlp()
#xs, ys = test_init_dataset()

#cost, tpr = evaluate(ws, bs, xs, ys)
#print('cost: {:.10f} tpr: {:.3f}'.format(cost, tpr))

#ws, bs = init_nrand([784, 30, 10], 0, 0.1)
#save_mlp(ws, bs, 'init.mlp')

#test_MNIST_avg_std()

#xs, ys = simple_dataset()
#ws, bs = load_mlp('simple_test.mlp')

fun = lambda x: (x - 1.23)**2


m = 1.4
# delta
step = 0.01

# initialize interval: [a, c]
a = -5.0
b = a + step
step *= m
c = b + step
step *= m
fa, fb, fc = fun(a), fun(b), fun(c)
if fa < fb < fc:
	while fa < fb:
		a, b, c = a - step, a, b
		step *= m
		fa, fb, fc = fun(a), fa, fb
else:
	while fb > fc:
		a, b, c = b, c, c + step
		step *= m
		fa, fb, fc = fb, fc, fun(c)

# invgr...1/golden_rate
invgr = (np.sqrt(5.0) - 1.0)/2.0
# invgr2...1/golden_rate**2
invgr2 = (3.0 - np.sqrt(5.0))/2.0

# golden section search
a, d = a, c
h = d - a
b, c = a + h*invgr2, a + h*invgr
fa, fb, fc, fd = fa, fun(b), fun(c), fc

tol = 1e-5
while h > tol:
	print(h)
	if fc < fb:
		h = d - b
		a, b, c = b, c, b + h*invgr
		fa, fb, fc = fb, fc, fun(c)
	else:
		h = c - a
		b, c, d = a + h*invgr2, b, c
		fb, fc, fd = fun(b), fb, fc

min_x = (a + d)/2.0

x = np.linspace(-5, 5, num=100)
y = fun(x)

plt.plot(x, y)
plt.plot(min_x, fun(min_x), 'ro')
plt.grid()
plt.show()


"""
ws, bs = load_mlp('mattmazur.mlp')
x = np.array((0.05, 0.1))
y = np.array((0.01, 0.99))
dws, dbs = backprop(ws, bs, x, y)

t = np.linspace(50, 80, num=20)
f = np.zeros(t.shape)

for i in range(len(t)):
	ws2 = [w - t[i]*dw for w, dw in zip(ws, dws)]
	bs2 = [b - t[i]*db for b, db in zip(bs, dbs)]
	f[i] = cost_x(ws2, bs2, x, y)[0]

plt.plot(t, f)
plt.show()
"""



