

def list_avg(a):
    return sum(a)/len(a)


a = [1.0, 6.0, 2.0, 5.0, 3.0, 3.0, 1.0, 2.0, 4.0, 2.0]
print(a)

m = 4
avg = 0.0

for i in range(len(a)):
    b = a[i-m+1:i+1] if i+1 > m else a[:i+1]
    avg = (avg + (a[i] - avg)/(i+1)) if i+1 <= m else (avg + (a[i] - a[i-m])/m)
    print(b, a[i], a[i-m], list_avg(b), avg)


