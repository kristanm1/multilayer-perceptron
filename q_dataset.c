#include "q_dataset.h"


q_dataset *alloc_q_dataset_from_dataset(dataset *set) {
    q_dataset *q_set = (q_dataset*) malloc(sizeof(q_dataset));
    q_set->size = set->size;
    unsigned int i, in_size = set->in[1] - set->in[0], out_size = set->out[1] - set->out[0];
    q_set->idcs = (unsigned int*) malloc(sizeof(unsigned int)*q_set->size);
    q_set->a_in = (long*) malloc(sizeof(long)*q_set->size*in_size);
    q_set->in = (long**) malloc(sizeof(long*)*q_set->size);
    q_set->a_out = (long*) malloc(sizeof(long)*q_set->size*out_size);
    q_set->out = (long**) malloc(sizeof(long*)*q_set->size);
    for(i = 0; i < q_set->size; i++) {
        q_set->idcs[i] = set->idcs[i];
        q_set->in[i] = q_set->a_in + i*in_size;
        unsigned int j;
        for(j = 0; j < in_size; j++) {
            q_set->in[i][j] = double_to_fixed(set->in[i][j]);
        }
        q_set->out[i] = q_set->a_out + i*out_size;
        for(j = 0; j < out_size; j++) {
            q_set->out[i][j] = double_to_fixed(set->out[i][j]);
        }
    }
    return q_set;
}

q_dataset *balanced_subset_q_dataset(q_dataset *q_set, unsigned int size) {
    unsigned int in_size = q_set->in[1] - q_set->in[0];
    unsigned int out_size = q_set->out[1] - q_set->out[0];
    q_dataset *q_subset = (q_dataset*) malloc(sizeof(q_dataset));
    q_subset->size = size;
    q_subset->idcs = (unsigned int*) malloc(sizeof(unsigned int)*size);
    q_subset->a_in = (long*) malloc(sizeof(long)*size*in_size);
    q_subset->in = (long**) malloc(sizeof(long*)*size);
    q_subset->a_out = (long*) malloc(sizeof(long)*size*out_size);
    q_subset->out = (long**) malloc(sizeof(long*)*size);
    unsigned int i, count[out_size], j = size%out_size, p = size/out_size;
    for(i = 0; i < out_size; i++) {
        count[i] = (j > 0 && i < j) ? p + 1 : p;
    }
    p = 0;
    for(i = 0; i < q_set->size; i++) {
        long *out = Q_GET_OUTPUT(q_set, i);
        unsigned int c = 0;
        for(j = 1; j < out_size; j++) {
            if(out[c] < out[j]) {
                c = j;
            }
        }
        if(count[c] > 0) {
            q_subset->idcs[p] = p;
            q_subset->in[p] = &q_subset->a_in[in_size*p];
            q_subset->out[p] = &q_subset->a_out[out_size*p];
            long *in = Q_GET_INPUT(q_set, i);
            for(j = 0; j < in_size; j++) {
                q_subset->in[p][j] = in[j];
            }
            for(j = 0; j < out_size; j++) {
                q_subset->out[p][j] = out[j];
            }
            p++;
            count[c]--;
        }
    }
    return q_subset;
}

void free_q_dataset(q_dataset *q_set) {
    free(q_set->idcs);
    free(q_set->in);
    free(q_set->a_in);
    free(q_set->out);
    free(q_set->a_out);
    free(q_set);
}

/**********************************************************************************************************************************/

void q_shuffle(q_dataset *q_set) {
    unsigned int i;
    for(i = q_set->size; i > 1; i--) {
        unsigned int r = (unsigned int) (rand()%i);
        if(r < i-1) {
            unsigned int temp = q_set->idcs[i-1];
            q_set->idcs[i-1] = q_set->idcs[r];
            q_set->idcs[r] = temp;
        }
    }
}

/**********************************************************************************************************************************/

void print_q_dataset(q_dataset *q_set) {
    unsigned int in_size = q_set->in[1] - q_set->in[0];
    unsigned int out_size = q_set->out[1] - q_set->out[0];
    unsigned int i, count[out_size];
    for(i = 0; i < out_size; i++) {
        count[i] = 0;
    }
    for(i = 0; i < q_set->size; i++) {
        unsigned int j, max = 0;
        long *out = Q_GET_OUTPUT(q_set, i);
        for(j = 1; j < out_size; j++) {
            if(out[max] < out[j]) {
                max = j;
            }
        }
        count[max]++;
    }
    printf("set->size = %u\n", q_set->size);
    printf(" in|%u -> out|%u\n", in_size, out_size);
    for(i = 0; i < out_size; i++) {
        printf("%2u -> %u\n", i, count[i]);
    }
}
