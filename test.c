#include "test.h"


dataset *alloc_matmazur_dataset() {
    dataset *set = (dataset*) malloc(sizeof(dataset));
    set->size = 1;
    set->idcs = (unsigned int*) malloc(sizeof(unsigned int));
    set->a_in = (double*) malloc(sizeof(double)*2);
    set->a_in[0] = 0.05; set->a_in[1] = 0.1;
    set->in = (double**) malloc(sizeof(double*)*2);
    set->in[0] = set->a_in; set->in[1] = set->a_in + 2;
    set->a_out = (double*) malloc(sizeof(double)*2);
    set->a_out[0] = 0.01; set->a_out[1] = 0.99;
    set->out = (double**) malloc(sizeof(double*)*2);
    set->out[0] = set->a_out; set->out[1] = set->a_out + 2;
    return set;
}

void test_mattmazur() {
    dataset *set = alloc_matmazur_dataset();
    mlp *nn = load_alloc_mlp("./weights/mattmazur.mlp");
    t_fwd *f = alloc_t_fwd(nn);
    t_bwd *b = alloc_t_bwd(nn);
    fun_tab *sigma_tab = alloc_fun_tab(-2, 2, 500, sigma);
    fun_tab *d_sigma_tab = alloc_fun_tab(-2, 2, 500, d_sigma);

    q_set_num_bits(6, 26);
    q_dataset *q_set = alloc_q_dataset_from_dataset(set);
    q_mlp *q_nn = alloc_q_mlp_from_mlp(nn);
    q_t_fwd *q_f = alloc_q_t_fwd(q_nn);
    q_t_bwd *q_b = alloc_q_t_bwd(q_nn);
    q_fun_tab *q_sigma_tab = alloc_q_fun_tab(-2, 2, 500, sigma);
    q_fun_tab *q_d_sigma_tab = alloc_q_fun_tab(-2, 2, 500, d_sigma);

    double cost, tpr;
    evaluate(nn, f, sigma_tab, set, &cost, &tpr);
    printf("MSE: %.9f\n", cost);
    backprop(nn, f, b, sigma_tab, d_sigma_tab, GET_INPUT(set, 0), GET_OUTPUT(set, 0));
    grad_desc(nn, nn, b, 0.5);
    print_mlp(nn);
    print_t_fwd(nn, f);

    q_evaluate(q_nn, q_f, q_sigma_tab, q_set, &cost, &tpr);
    printf("q_MSE: %.9f\n", cost);
    q_backprop(q_nn, q_f, q_b, q_sigma_tab, q_d_sigma_tab, Q_GET_INPUT(q_set, 0), Q_GET_OUTPUT(q_set, 0));
    q_grad_desc(q_nn, q_nn, q_b, double_to_fixed(0.5));
    print_q_mlp(q_nn);
    print_q_t_fwd(q_nn, q_f);

    free_dataset(set);
    free_mlp(nn);
    free_t_fwd(f);
    free_t_bwd(b);
    free_fun_tab(sigma_tab);
    free_fun_tab(d_sigma_tab);

    free_q_dataset(q_set);
    free_q_mlp(q_nn);
    free_q_t_fwd(q_f);
    free_q_t_bwd(q_b);
    free_q_fun_tab(q_sigma_tab);
    free_q_fun_tab(q_d_sigma_tab);
}

/**********************************************************************************************************************************/

dataset *alloc_XOR_dataset() {
    dataset *set = (dataset*) malloc(sizeof(dataset));
    set->size = 4;
    set->idcs = (unsigned int*) malloc(sizeof(unsigned int)*set->size);
    set->idcs[0] = 0; set->idcs[1] = 1; set->idcs[2] = 2; set->idcs[3] = 3;
    set->a_out = (double*) malloc(sizeof(double)*set->size);
    set->out = (double**) malloc(sizeof(double*)*set->size);
    set->a_out[0] = ZERO; set->a_out[1] = ONE; set->a_out[2] = ONE; set->a_out[3] = ZERO;
    set->out[0] = &set->a_out[0]; set->out[1] = &set->a_out[1];
    set->out[2] = &set->a_out[2]; set->out[3] = &set->a_out[3];
    set->a_in = (double*) malloc(sizeof(double)*set->size*2);
    set->in = (double**) malloc(sizeof(double*)*set->size);
    set->a_in[0] = 0.0f; set->a_in[1] = 0.0f; set->a_in[2] = 0.0f; set->a_in[3] = 1.0f;
    set->a_in[4] = 1.0f; set->a_in[5] = 0.0f; set->a_in[6] = 1.0f; set->a_in[7] = 1.0f;
    set->in[0] = &set->a_in[0]; set->in[1] = &set->a_in[2];
    set->in[2] = &set->a_in[4]; set->in[3] = &set->a_in[6];
    return set;
}

void test_xor(int argc, char *argv[]) {
    if(argc > 4) {
        unsigned int epochs = atoi(argv[1]);
        unsigned int batch_size = atoi(argv[2]);
        double learning_rate = atof(argv[3]);
        bits_cut = atoi(argv[4]);
        mlp *nn = load_alloc_mlp("./weights/xor.mlp");
        dataset *set = alloc_XOR_dataset();
        printf("set: cannot be splitted to train and valid set!\n");
        train_epochs_grad_desc(nn, epochs, batch_size, learning_rate, set);
        free_dataset(set);
        free_mlp(nn);
    }
}

/**********************************************************************************************************************************/

dataset *alloc_simple_dataset() {
    dataset *set = (dataset*) malloc(sizeof(dataset));
    set->size = 6;
    set->idcs = (unsigned int*) malloc(sizeof(unsigned int)*set->size);
    set->a_in = (double*) malloc(sizeof(double)*4*set->size);
    set->in = (double**) malloc(sizeof(double)*set->size);
    set->a_out = (double*) malloc(sizeof(double)*3*set->size);
    set->out = (double**) malloc(sizeof(double)*set->size);
    unsigned int i;
    for(i = 0; i < set->size; i++) {
        set->idcs[i] = i;
        set->in[i] = set->a_in + 4*i;
        set->out[i] = set->a_out + 3*i;
    }
    set->in[0][0] = 0; set->in[0][1] = 1; set->in[0][2] = 1; set->in[0][3] = 0;
    set->in[1][0] = 1; set->in[1][1] = 0; set->in[1][2] = 0; set->in[1][3] = 1;
    set->in[2][0] = 0; set->in[2][1] = 1; set->in[2][2] = 0; set->in[2][3] = 1;
    set->in[3][0] = 1; set->in[3][1] = 0; set->in[3][2] = 1; set->in[3][3] = 0;
    set->in[4][0] = 1; set->in[4][1] = 1; set->in[4][2] = 0; set->in[4][3] = 0;
    set->in[5][0] = 0; set->in[5][1] = 0; set->in[5][2] = 1; set->in[5][3] = 1;
    set->out[0][0] = 0.9; set->out[0][1] = 0.1; set->out[0][2] = 0.1;
    set->out[1][0] = 0.9; set->out[1][1] = 0.1; set->out[1][2] = 0.1;
    set->out[2][0] = 0.1; set->out[2][1] = 0.9; set->out[2][2] = 0.1;
    set->out[3][0] = 0.1; set->out[3][1] = 0.9; set->out[3][2] = 0.1;
    set->out[4][0] = 0.1; set->out[4][1] = 0.1; set->out[4][2] = 0.9;
    set->out[5][0] = 0.1; set->out[5][1] = 0.1; set->out[5][2] = 0.9;
    return set;
}

/**********************************************************************************************************************************/

dataset *alloc_random_dataset(unsigned int size, unsigned int in_size, unsigned int out_size) {
    dataset *set = (dataset*) malloc(sizeof(dataset));
    set->size = size;
    set->idcs = (unsigned int*) malloc(sizeof(unsigned int)*size);
    set->a_in = (double*) malloc(sizeof(double)*size*in_size);
    set->in = (double**) malloc(sizeof(double*)*size);
    set->a_out = (double*) malloc(sizeof(double)*size*out_size);
    set->out = (double**) malloc(sizeof(double*)*size);
    unsigned int i;
    for(i = 0; i < size; i++) {
        set->idcs[i] = i;
        set->in[i] = set->a_in + i*in_size;
        set->out[i] = set->a_out + i*out_size;
        unsigned int j;
        for(j = 0; j < in_size; j++) {
            set->in[i][j] = 2*(rand()/((double) RAND_MAX)) - 1;
        }
        for(j = 0; j < out_size; j++) {
            set->out[i][j] = 2*(rand()/((double) RAND_MAX)) - 1;
        }
    }
    return set;
}

/**********************************************************************************************************************************/

void test_rounder() {
    unsigned long di = (0x1L << 63) | (0x3ffL << 52) | 0xfffffffffffff;
    double d = *((double*) &di);
    int i;
    for(i = 0; i <= 52; i++) {
        bits_cut = i;
        double e = CUT_BITS_double(d);
        printf("cut: %2d | %.16g\n", i, e);
    }
    double a = 2, b = 0.12345678987654321;
    double r = a/b;
    for(i = 0; i <= 52; i++) {
        bits_cut = i;
        printf("%g\n", r - CUT_BITS_double(a/b));
    }
}

/**********************************************************************************************************************************/

void test_dataset() {
    char *labs_train = "./MNIST_unzipped/train-labels.idx1-ubyte";
    char *imgs_train = "./MNIST_unzipped/train-images.idx3-ubyte";
    dataset *set1_t = alloc_MNIST_dataset(labs_train, imgs_train);
    dataset *set2_t = balanced_subset_class_dataset(set1_t, 9876);
    print_class_dataset(set2_t);
    unsigned int i, p = 97, c = 0;
    double *in = GET_INPUT(set2_t, p);
    double *out = GET_OUTPUT(set2_t, p);
    for(i = 1; i < 10; i++) {
        if(out[c] < out[i]) {
            c = i;
        }
    }
    char filename[128];
    sprintf(filename, "test_%u.pgm", c);
    FILE *file = fopen(filename, "wb");
    fprintf(file, "P5\n28 28\n255\n");
    unsigned char buffer[784];
    for(i = 0; i < 784; i++) {
        buffer[i] = (unsigned char) (in[i] * 255.0);
    }
    fwrite(buffer, sizeof(unsigned char), 784, file);
    fclose(file);
    free_dataset(set2_t);
    free_dataset(set1_t);

}

void test_datasubset() {
    unsigned int size = 20, in_size = 5, out_size = 3;
    dataset *set = (dataset*) malloc(sizeof(dataset));
    set->size = size;
    set->idcs = (unsigned int*) malloc(sizeof(unsigned int)*size);
    set->a_in = (double*) malloc(sizeof(double)*size*in_size);
    set->in = (double**) malloc(sizeof(double*)*size);
    set->a_out = (double*) malloc(sizeof(double)*size*out_size);
    set->out = (double**) malloc(sizeof(double*)*size);
    unsigned int i;
    for(i = 0; i < size; i++) {
        set->idcs[i] = i;
        set->in[i] = set->a_in + i*in_size;
        set->out[i] = set->a_out + i*out_size;
        unsigned int j;
        for(j = 0; j < in_size; j++) {
            set->in[i][j] = i;
        }
        for(j = 0; j < out_size; j++) {
            set->out[i][j] = i + ((i+j)%out_size == 0);
        }
    }
    
    shuffle(set);
    for(i = 0; i < size; i++) {
        double *in = GET_INPUT(set, i), *out = GET_OUTPUT(set, i);
        unsigned int j;
        for(j = 0; j < in_size; j++) {
            printf("%2g ", in[j]);
        }
        printf(" -> ");
        for(j = 0; j < out_size; j++) {
            printf("%2g ", out[j]);
        }
        printf("\n");
    }
    printf("\n");

    dataset *subset1, *subset2;
    balanced_split_class_dataset(set, &subset1, &subset2, 1.0);

    for(i = 0; i < subset1->size; i++) {
        double *in = GET_INPUT(subset1, i), *out = GET_OUTPUT(subset1, i);
        unsigned int j;
        for(j = 0; j < in_size; j++) {
            printf("%2g ", in[j]);
        }
        printf(" -> ");
        for(j = 0; j < out_size; j++) {
            printf("%2g ", out[j]);
        }
        printf("\n");
    }
    printf("\n");
    for(i = 0; i < subset2->size; i++) {
        double *in = GET_INPUT(subset2, i), *out = GET_OUTPUT(subset2, i);
        unsigned int j;
        for(j = 0; j < in_size; j++) {
            printf("%2g ", in[j]);
        }
        printf(" -> ");
        for(j = 0; j < out_size; j++) {
            printf("%2g ", out[j]);
        }
        printf("\n");
    }
    printf("\n");

    print_class_dataset(set);
    print_class_dataset(subset1);
    print_class_dataset(subset2);

    free_dataset(subset1);
    free_dataset(subset2);
    free_dataset(set);
}

void test_q_dataset() {
    char *labs_train = "./MNIST_unzipped/train-labels.idx1-ubyte";
    char *imgs_train = "./MNIST_unzipped/train-images.idx3-ubyte";
    dataset *set = alloc_MNIST_dataset(labs_train, imgs_train);
    q_set_num_bits(10, 10);
    q_dataset *q_set1_t = alloc_q_dataset_from_dataset(set);
    free_dataset(set);
    q_dataset *q_set2_t = balanced_subset_q_dataset(q_set1_t, 9876);

    print_q_dataset(q_set2_t);
    unsigned int i, p = 97, c = 0;
    long *in = Q_GET_INPUT(q_set2_t, p);
    long *out = Q_GET_OUTPUT(q_set2_t, p);
    for(i = 1; i < 10; i++) {
        if(out[c] < out[i]) {
            c = i;
        }
    }
    char filename[128];
    sprintf(filename, "test_%u.pgm", c);
    FILE *file = fopen(filename, "wb");
    double max = fixed_to_double(sat_max);
    fprintf(file, "P5\n28 28\n%u\n", (max < 255.0) ? ((unsigned char) max) : 255);
    unsigned char buffer[784];
    for(i = 0; i < 784; i++) {
        buffer[i] = (unsigned char) (fixed_to_double(in[i])*255.0);
    }
    fwrite(buffer, sizeof(unsigned char), 784, file);
    fclose(file);
    free_q_dataset(q_set2_t);
    free_q_dataset(q_set1_t);
}

/**********************************************************************************************************************************/

double one_over_x(double x) {
    return 1.0/x;
}

void test_fixed_point() {

    q_set_num_bits(6, 20);

    printf("FIXED POINT:\n");
    printf("range: [%g, %g]\n", fixed_to_double(sat_min), fixed_to_double(sat_max));
    printf("precision %g\n", fixed_to_double(1L));

    double a, b, c;
    unsigned long q_a, q_b, q_c, q_c2, q_c3;

    printf("-----------------mul------------------------\n");
    a = 5.97;
    q_a = double_to_fixed(a);
    b = -2.32;
    q_b = double_to_fixed(b);
    c = a*b;
    q_c = q_mul(q_a, q_b);
    printf("floating point -> a: %10.6g b: %10.6g a*b: %10.6g\n", a, b, c);
    printf("   fixed point -> a: %10.6g b: %10.6g a*b: %10.6g\n", fixed_to_double(q_a), fixed_to_double(q_b), fixed_to_double(q_c));

    printf("-----------------div------------------------\n");
    a = 7.87;
    q_a = double_to_fixed(a);
    b = 3.23;
    q_b = double_to_fixed(b);
    c = a/b;
    q_c = q_p_div(q_a, q_b);
    q_c2 = q_div(q_a, q_b);
    printf("floating point -> a: %10.6g b: %10.6g a/b: %10.6g\n", a, b, c);
    printf("   fixed point -> a: %10.6g b: %10.6g a/b: %10.6g a/b: %10.6g\n", fixed_to_double(q_a), fixed_to_double(q_b), fixed_to_double(q_c), fixed_to_double(q_c2));

    printf("----------------sqrt------------------------\n");
    a = 21.37;
    q_a = double_to_fixed(a);
    c = sqrt(a);
    q_c = q_sqrt(q_a);
    printf("floating point -> a: %10.6g sqrt(a): %10.6g\n", a, c);
    printf("   fixed point -> a: %10.6g sqrt(a): %10.6g\n", fixed_to_double(q_a), fixed_to_double(q_c));

}

/**********************************************************************************************************************************/

double f1(double x) {
    return 4.0*x - 5.0;
}

double f2(double x) {
    return 4.0*x*x - 5.0;
}

void test_fun_tab() {

    q_set_num_bits(10, 22);
    char *name = "test.tab";
    char *q_name = "test.q_tab";

    double start = -10.0, end = 10.0;
    unsigned int size = 512;

    double (*fun)(double);
    //fun = f1;
    fun = f2;
    //fun = one_over_x;
    //fun = sqrt;
    //fun = exp;
    //fun = sigma;
    //fun = d_sigma;

    fun_tab *tab = alloc_fun_tab(start, end, size, fun);
    //save_fun_tab(name, tab);
    //free_fun_tab(tab);
    //tab = alloc_load_fun_tab(name);

    q_fun_tab *q_tab = alloc_q_fun_tab(start, end, size, fun);
    //save_q_fun_tab(q_name, q_tab);
    //free_q_fun_tab(q_tab);
    //q_tab = alloc_load_q_fun_tab(q_name);
    

    double x = -7.127, dx = 0.751;
    int i;
    for(i = 0; i < 20; i++) {
        double y = fun(x);
        double y2 = f_fun_tab(x, tab);

        long q_x = double_to_fixed(x);
        long q_y2 = f_q_fun_tab(q_x, q_tab);
        
        printf("%11.4g[%11.4g]: %11.4g %11.4g[%11.4g]\n", x, fixed_to_double(q_x), y, y2, fixed_to_double(q_y2));
        x += dx;
    }

    free_fun_tab(tab);
    free_q_fun_tab(q_tab);

}

/**********************************************************************************************************************************/

void test_MNIST_dataset_and_q_dataset() {

    char *labs_train = "./MNIST_unzipped/train-labels.idx1-ubyte";
    char *imgs_train = "./MNIST_unzipped/train-images.idx3-ubyte";

    q_set_num_bits(4, 28);
    dataset *set = alloc_MNIST_dataset(labs_train, imgs_train);
    q_dataset *q_set = alloc_q_dataset_from_dataset(set);

    int i;
    for(i = 0; i < 10; i++) {
        double *in = GET_INPUT(set, i), *out = GET_OUTPUT(set, i);
        long *q_in = Q_GET_INPUT(q_set, i), *q_out = Q_GET_OUTPUT(q_set, i);
        double e_in = 0.0, e_out = 0.0;
        int j;
        for(j = 0; j < 784; j++) {
            e_in += my_abs(in[j] - fixed_to_double(q_in[j]));
        }
        for(j = 0; j < 10; j++) {
            e_out += my_abs(out[j] - fixed_to_double(q_out[j]));
        }
        printf("i: %4d| err. in: %10.3e err. out: %10.3e\n", i, e_in, e_out);
    }

    free_dataset(set);
    free_q_dataset(q_set);

}

void test_mattmazur_mlp_and_q_mlp() {
    dataset *set = alloc_matmazur_dataset();
    fun_tab *sigma_tab = alloc_fun_tab(-2, 2, 1000, sigma);
    mlp *nn = load_alloc_mlp("./weights/mattmazur.mlp");
    t_fwd *f = alloc_t_fwd(nn);
    t_bwd *b = alloc_t_bwd(nn);
    double *in = GET_INPUT(set, 0), *out = GET_OUTPUT(set, 0);

    q_set_num_bits(4, 28);
    q_dataset *q_set = alloc_q_dataset_from_dataset(set);
    q_fun_tab *q_sigma_tab = alloc_q_fun_tab(-2, 2, 1000, sigma);
    q_mlp *q_nn = alloc_q_mlp_from_mlp(nn);
    q_t_fwd *q_f = alloc_q_t_fwd(q_nn);
    q_t_bwd *q_b = alloc_q_t_bwd(q_nn);
    long *q_in = Q_GET_INPUT(q_set, 0), *q_out = Q_GET_OUTPUT(q_set, 0);

    unsigned int epochs = 10, batch_size = 1;
    double learning_rate = 0.5;
    printf("set: cannot be splitted to train and valid set!\n");
    train_epochs_grad_desc(nn, epochs, batch_size, learning_rate, set);
    q_train_epochs_grad_desc(q_nn, epochs, batch_size, double_to_fixed(learning_rate), set);

    double *a = forward(nn, f, sigma_tab, in);
    long *q_a = q_forward(q_nn, q_f, q_sigma_tab, q_in);

    int i;
    for(i = 0; i < 2; i++) {
        printf("%10.5g : %10.5g\n", a[i], fixed_to_double(q_a[i]));
    }

    free_dataset(set);
    free_fun_tab(sigma_tab);
    free_mlp(nn);
    free_t_fwd(f);
    free_t_bwd(b);

    free_q_dataset(q_set);
    free_q_fun_tab(q_sigma_tab);
    free_q_mlp(q_nn);
    free_q_t_fwd(q_f);
    free_q_t_bwd(q_b);
}

void test_random_mlp_and_q_mlp() {

    int example = 9;

    unsigned int dataset_size = 10;
    unsigned int size = 3, layers[] = {4, 3, 3};

    dataset *set = alloc_random_dataset(dataset_size, layers[0], layers[size-1]);
    fun_tab *sigma_tab = alloc_fun_tab(-11, 11, 10000, sigma);
    fun_tab *d_sigma_tab = alloc_fun_tab(-11, 11, 10000, d_sigma);
    mlp *nn = alloc_mlp(size, layers);
    init_rand_mlp(nn, 1.0);
    t_fwd *f = alloc_t_fwd(nn);
    t_bwd *b = alloc_t_bwd(nn);
    double *in = GET_INPUT(set, example), *out = GET_OUTPUT(set, example);

    q_set_num_bits(6, 26);
    q_dataset *q_set = alloc_q_dataset_from_dataset(set);
    q_fun_tab *q_sigma_tab = alloc_q_fun_tab(-11, 11, 10000, sigma);
    q_fun_tab *q_d_sigma_tab = alloc_q_fun_tab(-11, 11, 10000, sigma);
    q_mlp *q_nn = alloc_q_mlp_from_mlp(nn);
    q_t_fwd *q_f = alloc_q_t_fwd(q_nn);
    q_t_bwd *q_b = alloc_q_t_bwd(q_nn);
    long *q_in = Q_GET_INPUT(q_set, example), *q_out = Q_GET_OUTPUT(q_set, example);

    unsigned int epochs = 10, batch_size = 2;
    double learning_rate = 0.4;


    forward(nn, f, sigma_tab, in);
    q_forward(q_nn, q_f, q_sigma_tab, q_in);

    print_t_fwd(nn, f);
    print_q_t_fwd(q_nn, q_f);

    free_dataset(set);
    free_fun_tab(sigma_tab);
    free_fun_tab(d_sigma_tab);
    free_mlp(nn);
    free_t_fwd(f);
    free_t_bwd(b);

    free_q_dataset(q_set);
    free_q_fun_tab(q_sigma_tab);
    free_q_fun_tab(q_d_sigma_tab);
    free_q_mlp(q_nn);
    free_q_t_fwd(q_f);
    free_q_t_bwd(q_b);

}

void test_batch_derivative() {

    bits_cut = 0;
    q_set_num_bits(10, 22);

    unsigned int batch_size = 10, batch_start = 0;
    double learning_rate = 0.1;
    long q_learning_rate = double_to_fixed(0.1);

    char *labs_train = "./MNIST_unzipped/train-labels.idx1-ubyte";
    char *imgs_train = "./MNIST_unzipped/train-images.idx3-ubyte";

    mlp *nn = load_alloc_mlp("weights/init.mlp");
    t_fwd *f = alloc_t_fwd(nn);
    t_bwd *b = alloc_t_bwd(nn), *grad = alloc_t_bwd(nn);

    dataset *train_set = alloc_MNIST_dataset(labs_train, imgs_train);
    dataset *train_subset = balanced_subset_class_dataset(train_set, 10000);
    free_dataset(train_set);
    train_set = train_subset;

    fun_tab *sigma_tab = alloc_fun_tab(fun_tab_low, fun_tab_high, fun_tab_size, sigma);
    fun_tab *d_sigma_tab = alloc_fun_tab(fun_tab_low, fun_tab_high, fun_tab_size, d_sigma);


    q_mlp *q_nn = alloc_q_mlp_from_mlp(nn);
    q_t_fwd *q_f = alloc_q_t_fwd(q_nn);
    q_t_bwd *q_b = alloc_q_t_bwd(q_nn), *q_grad = alloc_q_t_bwd(q_nn);

    q_dataset *q_train_set = alloc_q_dataset_from_dataset(train_set);
    
    q_fun_tab *q_sigma_tab = alloc_q_fun_tab(fun_tab_low, fun_tab_high, fun_tab_size, sigma);
    q_fun_tab *q_d_sigma_tab = alloc_q_fun_tab(fun_tab_low, fun_tab_high, fun_tab_size, d_sigma);

    batch_derivative(nn, f, grad, b, sigma_tab, d_sigma_tab, batch_size, batch_start, train_set);
    q_batch_derivative(q_nn, q_f, q_grad, q_b, q_sigma_tab, q_d_sigma_tab, batch_size, batch_start, q_train_set);

    double avg_abs_err = 0.0;
    unsigned int i, all_par = 0;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            double temp1, temp2;
            for(k = 0; k < nn->layers[i-1]; k++) {
                temp1 = fixed_to_double(q_grad->weights[i-1][j][k]);
                temp2 = my_abs(grad->weights[i-1][j][k] - temp1);
                //printf("%10.6f %10.6f | %10.6f\n", grad->weights[i-1][j][k], temp1, temp2);
                avg_abs_err += temp2;
                all_par++;
            }
            temp1 = fixed_to_double(q_grad->biases[i-1][j]);
            temp2 = my_abs(grad->biases[i-1][j] - temp1);
            //printf("%10.6f %10.6f | %10.6f\n\n", grad->biases[i-1][j], temp1, temp2);
            avg_abs_err += temp2;
            all_par++;
        }
    }
    printf("avg. abs. err.: %g\n", avg_abs_err/all_par);

    free_mlp(nn);
    free_t_fwd(f);
    free_t_bwd(b); free_t_bwd(grad);
    free_fun_tab(sigma_tab); free_fun_tab(d_sigma_tab);
    free_dataset(train_set);

    free_q_mlp(q_nn);
    free_q_t_fwd(q_f);
    free_q_t_bwd(q_b); free_q_t_bwd(q_grad);
    free_q_fun_tab(q_sigma_tab); free_q_fun_tab(q_d_sigma_tab);
    free_q_dataset(q_train_set);

}

/**********************************************************************************************************************************/

void test_avg_std() {
    dataset *set = alloc_simple_dataset();
    unsigned int size = 3, layers[] = {4, 3, 3};
    mlp *nn = alloc_mlp(size, layers);
    //fun_tab *tab = alloc_load_fun_tab("exp_tab.tab");
    fun_tab *sigma_tab = alloc_fun_tab(-10, 10, 10000, sigma);
    fun_tab *d_sigma_tab = alloc_fun_tab(-10, 10, 10000, d_sigma);
    t_fwd *f = alloc_t_fwd(nn);
    t_bwd *b = alloc_t_bwd(nn);
    init_rand_mlp(nn, 0.1);
    //save_mlp(nn, "weights/simple_test.mlp");
    reset_stat_mlp(nn);
    reset_stat_t_fwd(nn, f);
    reset_stat_t_bwd(nn, b);
    //print_mlp(nn);
    double array[set->size];
    int i;
    for(i = 0; i < set->size; i++) {
        double *in = GET_INPUT(set, i), *out = GET_OUTPUT(set, i);
        backprop(nn, f, b, sigma_tab, d_sigma_tab, in, out);
        grad_desc(nn, nn, b, 0.5);
        //array[i] = nn->weights[1][1][2]; //save from mlp
        //array[i] = f->z_activations[1][1]; // save from t_fwd
        array[i] = b->weights[1][1][2]; // save from t_bwd
        printf("%.1g %.1g %.1g %.1g -> %.1g %.1g %.1g\n", in[0], in[1], in[2], in[3], out[0], out[1], out[2]);
        //print_t_fwd(nn, f);
        //print_t_bwd(nn, b);
        //printf("\n");
    }
    //print_stat_mlp(nn);
    //print_stat_t_fwd(nn, f);
    print_stat_t_bwd(nn, b);
    double mean = array_mean(set->size, array);
    double sd = array_sd(set->size, mean, array);
    printf("mean: %g sd: %g min: %g max: %g\n", mean, sd, array[my_argmin(set->size, array)], array[my_argmax(set->size, array)]);
    free_dataset(set);
    free_mlp(nn);
    free_fun_tab(sigma_tab);
    free_fun_tab(d_sigma_tab);
    free_t_fwd(f);
    free_t_bwd(b);
}

void test_MNIST_avg_std() {

    unsigned int size = 3, layers[] = {784, 30, 10};
    char *labs_train = "./MNIST_unzipped/train-labels.idx1-ubyte";
    char *imgs_train = "./MNIST_unzipped/train-images.idx3-ubyte";

    dataset *set = alloc_MNIST_dataset(labs_train, imgs_train);
    mlp *nn = load_alloc_mlp("weights/init.mlp");
    t_fwd *f = alloc_t_fwd(nn);
    t_bwd *b = alloc_t_bwd(nn);

    fun_tab *sigma_tab = alloc_fun_tab(-10, 10, 10000, sigma);
    fun_tab *d_sigma_tab = alloc_fun_tab(-10, 10, 10000, d_sigma);

    int test_size = set->size;
    double array[test_size];

    reset_stat_t_fwd(nn, f);

    int i;
    for(i = 0; i < test_size; i++) {
        double *in = GET_INPUT(set, i), *out = GET_OUTPUT(set, i);
        backprop(nn, f, b, sigma_tab, d_sigma_tab, in, out);
        array[i] = f->z_activations[0][0];
    }

    double mean = array_mean(test_size, array);
    double sd = array_sd(test_size, mean, array);
    printf("mean: %g sd: %g min: %g max: %g\n", mean, sd, array[my_argmin(test_size, array)], array[my_argmax(test_size, array)]);
    print_stat_t_fwd(nn, f);

    free_dataset(set);
    free_mlp(nn);
    free_t_fwd(f);
    free_t_bwd(b);
    free_fun_tab(sigma_tab);
    free_fun_tab(d_sigma_tab);

}

/**********************************************************************************************************************************/

void test_q_xor(int argc, char **argv) {
    if(argc > 5) {
        unsigned int epochs = atoi(argv[1]);
        unsigned int batch_size = atoi(argv[2]);
        q_set_num_bits(atoi(argv[4]), atoi(argv[5]));
        long q_learning_rate = double_to_fixed(atof(argv[3]));

        mlp *nn = load_alloc_mlp("./weights/xor.mlp");
        q_mlp *q_nn = alloc_q_mlp_from_mlp(nn);
        free_mlp(nn);

        dataset *set = alloc_XOR_dataset();
        printf("set: cannot be splitted to train and valid set!\n");
        q_train_epochs_grad_desc(q_nn, epochs, batch_size, q_learning_rate, set);
        
        free_q_mlp(q_nn);
        free_dataset(set);
    }
}

/**********************************************************************************************************************************/

void test_moving_average() {

    int size = 4;
    double array[size], mean_w = 0.0, M_w = 0.0;

    int i;
    for(i = 0; i < size; i++) {
        array[i] = (double) my_round(100*(rand()/((double) RAND_MAX)) - 50.0);
        printf("%3d| %g\n", i, array[i]);

        double temp1 = mean_w + (array[i] - mean_w)/(i+1);
        M_w = M_w + (array[i] - mean_w)*(array[i] - temp1);
        mean_w = temp1;

    }
    double sd_w = sqrt(M_w/size);

    double mean = array_mean(i, array);
    double sd = array_sd(i, mean, array);
    printf("%10.7g %10.7g\n", mean, sd);
    printf("%10.7g %10.7g\n", mean_w, sd_w);

}

/**********************************************************************************************************************************/

void test_multipliers() {

    long a, b, c, c2, c3, ae2, ae3;
    double re2, re3;

    a = 234L; b = -198L;
    c = a*b; c2 = long_ma_mul(a, b); c3 = long_ilm(a, b);
    ae2 = c - c2; ae3 = c - c3;
    re2 = ((double) ae2) / c; re3 = ((double) ae3) / c;
    printf("%ld * %ld = %ld : %ld[%.3g] %ld[%.3g]\n", a, b, c, c2, re2, c3, re3);

    a = -164L; b = 83L;
    c = a*b; c2 = long_ma_mul(a, b); c3 = long_ilm(a, b);
    ae2 = c - c2; ae3 = c - c3;
    re2 = ((double) ae2) / c; re3 = ((double) ae3) / c;
    printf("%ld * %ld = %ld : %ld[%.3g] %ld[%.3g]\n", a, b, c, c2, re2, c3, re3);

    a = -12288L; b = -12288L;
    c = a*b; c2 = long_ma_mul(a, b); c3 = long_ilm(a, b);
    ae2 = c - c2; ae3 = c - c3;
    re2 = ((double) ae2) / c; re3 = ((double) ae3) / c;
    printf("%ld * %ld = %ld : %ld[%.3g] %ld[%.3g]\n", a, b, c, c2, re2, c3, re3);


}

/**********************************************************************************************************************************/

void test_gss() {

    bits_cut = 0;
    dataset *set = alloc_matmazur_dataset();
    mlp *nn = load_alloc_mlp("./weights/mattmazur.mlp");
    mlp *nn2 = alloc_mlp(nn->size, nn->layers);
    t_fwd *f = alloc_t_fwd(nn);
    t_bwd *b = alloc_t_bwd(nn);
    fun_tab *sigma_tab = alloc_fun_tab(-2, 2, 500, sigma);
    fun_tab *d_sigma_tab = alloc_fun_tab(-2, 2, 500, d_sigma);


    backprop(nn, f, b, sigma_tab, d_sigma_tab, GET_INPUT(set, 0), GET_OUTPUT(set, 0));

    double min = gss(nn, nn2, f, b, sigma_tab, set), cost, tpr;

    grad_desc(nn, nn, b, min);
    evaluate(nn, f, sigma_tab, set, &cost, &tpr);

    printf("min: %g\n", min);
    printf("cost: %g\n", cost);
    printf("tpr: %g\n", tpr);


    free_dataset(set);
    free_mlp(nn);
    free_mlp(nn2);
    free_t_fwd(f);
    free_t_bwd(b);
    free_fun_tab(sigma_tab);
    free_fun_tab(d_sigma_tab);

}

/**********************************************************************************************************************************/



/**********************************************************************************************************************************/
