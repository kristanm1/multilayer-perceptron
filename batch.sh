#!/bin/bash

#SBATCH --job-name=mlp
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --time=5:00:00
#SBATCH --output=mlp_%a.out
#SBATCH --array=0-7

epochs=$1
batch_size=$2
learning_rate=$3
bits_cut=(0 10 20 30 35 40 45 50)

#./compile.sh
./main $epochs $batch_size $learning_rate ${bits_cut[$SLURM_ARRAY_TASK_ID]}

