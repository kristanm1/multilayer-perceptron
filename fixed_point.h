#ifndef FIXED_POINT_INCLUDE
#define FIXED_POINT_INCLUDE

    #include "utils.h"

/********************************************************************************
    Q (number format): https://en.wikipedia.org/wiki/Q_(number_format)#Float_to_Q
*********************************************************************************
*********************************************************************************/

    typedef struct q_fun_tab q_fun_tab;

    int integer_bits, fraction_bits;
    long sat_max, sat_min;

    void q_set_num_bits(int, int);

    long q_add(long, long);
    long q_sub(long, long);
    long q_mul(long, long);

    // x0 = 48/17 - 32/17*D => Newton–Raphson division (4 iterations)
    long q_div(long, long);
    // provided division: / operator
    long q_p_div(long, long);

    // S = a * 2^2n => sqrt(S) = sqrt(a) * 2^n
    // estimate sqrt as: sqrt(S) ~= (0.5 + 0.5*a) * 2^n
    long q_es_sqrt(long);
    // x0 = q_es_sqrt => Babylonian method (4 iterations)
    long q_sqrt(long);

    double fixed_to_double(long);
    long double_to_fixed(double);

#endif