#ifndef MUL_PERCEPTRON_INCLUDE
#define MUL_PERCEPTRON_INCLUDE

    #include <math.h>
    #include <time.h>

    #include "utils.h"
    #include "dataset.h"
    #include "rounder.h"
    #include "optimizer.h"

/********************************************************************************
    multilayer perceptron: http://neuralnetworksanddeeplearning.com/chap2.html
*********************************************************************************
*********************************************************************************/

    #define MAX_FILENAME_LENGTH 128


    typedef struct fun_tab fun_tab;
    typedef struct t_der t_der;
    typedef struct t_der_adadelta t_der_adadelta;

    // size: 1 (for input layer) + number of layers
    // layers[i]: number of neurons per layer i
    // weights, biases
    typedef struct mlp {
        unsigned int size, *layers;
        // weights and biases
        double *a_w, **a_wr, ***weights, *a_b, **biases;
        // statistics
        unsigned int i;
        double *a_w_means, *a_w_sd, *a_w_min, *a_w_max, **a_wr_means, **a_wr_sd, **a_wr_min, **a_wr_max, ***w_means, ***w_sd, ***w_min, ***w_max;
        double *a_b_means, *a_b_sd, *a_b_min, *a_b_max, **b_means, **b_sd, **b_min, **b_max;
    } mlp;

    // (z_)activations: temporary hold activations in forward pass and some statistics (means, standard deviation, min, max)
    typedef struct t_fwd {
        // activations
        double *z, *a, **z_activations, **activations;
        // statistics
        unsigned int i;
        double *a_z_means, *a_z_sd, *a_z_min, *a_z_max, **z_means, **z_sd, **z_min, **z_max;
    } t_fwd;

    // weights, biases: temporary hold derivatives in backward pass and some statistics (means, standard deviation, min, max)
    typedef struct t_bwd {
        // weights and biases
        double *a_w, **a_wr, ***weights, *a_b, **biases;
        // statistics
        unsigned int i;
        double *a_w_means, *a_w_sd, *a_w_min, *a_w_max, **a_wr_means, **a_wr_sd, **a_wr_min, **a_wr_max, ***w_means, ***w_sd, ***w_min, ***w_max;
        double *a_b_means, *a_b_sd, *a_b_min, *a_b_max, **b_means, **b_sd, **b_min, **b_max;
    } t_bwd;

    int bits_cut;
    extern double fun_tab_low, fun_tab_high;
    extern unsigned int fun_tab_size;

    double sigma(double);
    double d_sigma(double);

    // allocate and return pointer to mlp
    mlp *alloc_mlp(unsigned int, unsigned int*);
    void free_mlp(mlp*);
    // initialize random values from [-1, 1] to mlp weights, biases
    void init_rand_mlp(mlp*, double);
    void init_mlp_from_mlp(mlp*, mlp*);
    void print_mlp(mlp*);
    void reset_stat_mlp(mlp*);
    void print_stat_mlp(mlp*);
    void add_stat_mlp(mlp*);
    // save mlp to disk
    void save_mlp(mlp*, char*);
    // allocate, load and return pointer to mlp
    mlp *load_alloc_mlp(char*);
    // generate name from arguments and mlp attributes to char pointer(last argument)
    char *gen_name_mlp(mlp*, char*, double, unsigned int, char*);
    int nan_val_mlp(mlp*);
    
    // allocate t_fwd for forward pass
    t_fwd *alloc_t_fwd(mlp*);
    void free_t_fwd(t_fwd*);
    void print_t_fwd(mlp*, t_fwd*);
    void reset_stat_t_fwd(mlp*, t_fwd*);
    void print_stat_t_fwd(mlp*, t_fwd*);
    void add_stat_t_fwd(mlp*, t_fwd*);
    int nan_val_t_fwd(mlp*, t_fwd*);

    // allocate t_bwd for backward pass
    t_bwd *alloc_t_bwd(mlp*);
    void free_t_bwd(t_bwd*);
    void print_t_bwd(mlp*, t_bwd*);
    void reset_stat_t_bwd(mlp*, t_bwd*);
    void print_stat_t_bwd(mlp*, t_bwd*);
    void add_stat_t_bwd(mlp*, t_bwd*);
    int nan_val_t_bwd(mlp*, t_bwd*);
    // set all derivatives to zero
    void reset_t_bwd(mlp*, t_bwd*);
    // add one derivative to another derivative
    void add_t_bwd(mlp*, t_bwd*, t_bwd*);
    // add multiplied derivative to another derivative
    void mul_add_t_bwd(mlp*, t_bwd*, t_bwd*, double);
    // gradient L2 norm
    double normL2_t_bwd(mlp*, t_bwd*);

    // forward pass
    double *forward(mlp*, t_fwd*, fun_tab*, double*);
    // backward pass, after function call -> t_bwd contains derivative
    void backprop(mlp*, t_fwd*, t_bwd*, fun_tab*, fun_tab*, double*, double*);

    void cost_x(mlp*, t_fwd*, fun_tab*, double*, double*, double*, int*);
    // evaluate mlp's cost function and tpr(true positive rate|accuracy)
    void evaluate(mlp*, t_fwd*, fun_tab*, dataset*, double*, double*);

    void init_for_train(mlp*, mlp**, mlp**, t_fwd**, t_bwd**, t_bwd**, fun_tab**, fun_tab**);
    void free_from_train(mlp*, mlp*, mlp*, t_fwd*, t_bwd*, t_bwd*, fun_tab*, fun_tab*);
    void batch_derivative(mlp*, t_fwd*, t_bwd*, t_bwd*, fun_tab*, fun_tab*, unsigned int, unsigned int, dataset*);

    void batch_grad_desc(mlp*, t_fwd*, t_bwd*, t_bwd*, fun_tab*, fun_tab*, unsigned int, unsigned int, double, dataset*);
    void batch_gss(mlp*, mlp*, t_fwd*, t_bwd*, t_bwd*, fun_tab*, fun_tab*, unsigned int, unsigned int, dataset*);
    void batch_adam(mlp*, t_fwd*, t_bwd*, t_bwd*, t_bwd*, t_bwd*, fun_tab*, fun_tab*, unsigned int, unsigned int, double, double, double, double, double, dataset*);
    
    // train mlp: number of epochs is give, remember best performing *weights and *biases
    //            set mlp weights and biases to *weights and *biases
    void train_epochs_grad_desc(mlp*, unsigned int, unsigned int, double, dataset*);
    void train_epochs_gss(mlp*, unsigned int, unsigned int, dataset*);
    void train_epochs_adam(mlp*, unsigned int, unsigned int, double, double, double, dataset*);

    // train mlp: split train set to train subset and valid set (80%, 20%)
    //            train until cost function grows twice in a row, remember best performing *weights and *biases
    //            set mlp weights and biases to *weights and *biases
    void train_grad_desc(mlp*, unsigned int, double, dataset*);
    void train_adam(mlp*, unsigned int, unsigned int, double, double, double, dataset*);

#endif