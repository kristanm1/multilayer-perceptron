#include "optimizer.h"


/**********************************************************************************************************************************/

void grad_desc(mlp *src, mlp *dst, t_bwd *grad, double learning_rate) {
   unsigned int i;
    for(i = 1; i < src->size; i++) {
        unsigned int j;
        for(j = 0; j < src->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < src->layers[i-1]; k++) {
                dst->weights[i-1][j][k] = CUT_BITS_double(src->weights[i-1][j][k] - learning_rate*grad->weights[i-1][j][k]);
            }
            dst->biases[i-1][j] = CUT_BITS_double(src->biases[i-1][j] - learning_rate*grad->biases[i-1][j]);
        }
    }
}

void adam(mlp *nn, t_bwd *grad, t_bwd *m, t_bwd *v, double learning_rate, double b1, double b2, double pow_b1, double pow_b2) {
    unsigned int i;
    double a = learning_rate*sqrt(1-pow_b2)/(1-pow_b1);
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                m->weights[i-1][j][k] = CUT_BITS_double(b1*m->weights[i-1][j][k] + (1-b1)*grad->weights[i-1][j][k]);
                v->weights[i-1][j][k] = CUT_BITS_double(b2*v->weights[i-1][j][k] + (1-b2)*grad->weights[i-1][j][k]*grad->weights[i-1][j][k]);
                nn->weights[i-1][j][k] = CUT_BITS_double(nn->weights[i-1][j][k] - a*m->weights[i-1][j][k]/(sqrt(v->weights[i-1][j][k]) + adam_epsilon));
            }
            m->biases[i-1][j] = CUT_BITS_double(b1*m->biases[i-1][j] + (1-b1)*grad->biases[i-1][j]);
            v->biases[i-1][j] = CUT_BITS_double(b2*v->biases[i-1][j] + (1-b2)*grad->biases[i-1][j]*grad->biases[i-1][j]);
            nn->biases[i-1][j] = CUT_BITS_double(nn->biases[i-1][j] - a*m->biases[i-1][j]/(sqrt(v->biases[i-1][j]) + adam_epsilon));
        }
    }
}


/**********************************************************************************************************************************/

double gss(mlp *nn, mlp *nn2, t_fwd *f, t_bwd *grad, fun_tab *sigma_tab, dataset *set) {

    double a, b, c, d, fa, fb, fc, fd, h, step = gss_step, tpr;

    a = 0.0;
    b = step;
    step *= gss_mul;
    c = b + step;

    evaluate(nn, f, sigma_tab, set, &fa, &tpr);
    //printf("init. a: %g fa: %g\n", a, fa);

    grad_desc(nn, nn2, grad, b);
    evaluate(nn2, f, sigma_tab, set, &fb, &tpr);
    //printf("init. b: %g fb: %g\n", b, fb);

    grad_desc(nn, nn2, grad, c);
    evaluate(nn2, f, sigma_tab, set, &fc, &tpr);
    //printf("init. c: %g fc: %g\n", c, fc);

    if(fa < fb && fb < fc) {
        while(fa <= fb) {
            c = b; fc = fb;
            b = a; fb = fa;
            a = a - step;
            grad_desc(nn, nn2, grad, a);
            evaluate(nn2, f, sigma_tab, set, &fa, &tpr);
            //printf("new a: %g fa: %g\n", a, fa);
            step *= gss_mul;
        }
    } else {
        while(fc <= fb) {
            a = b; fa = fb;
            b = c; fb = fc;
            c = c + step;
            grad_desc(nn, nn2, grad, c);
            evaluate(nn2, f, sigma_tab, set, &fc, &tpr);
            //printf("new c: %g fc: %g\n", a, fa);
            step *= gss_mul;
        }
    }

    //printf("init. int: [%g, %g]\n", a, c);

    d = c; fd = fc;
    h = d - a;
    b = a + h*INV_SQ_GOLD_RATIO;
    c = a + h*INV_GOLD_RATIO;

    grad_desc(nn, nn2, grad, b);
    evaluate(nn2, f, sigma_tab, set, &fb, &tpr);

    grad_desc(nn, nn2, grad, c);
    evaluate(nn2, f, sigma_tab, set, &fc, &tpr);

    while(h > gss_tol) {
        if(fb < fc) {
            h = c - a;
            d = c; fd = fc;
            c = b; fc = fb;
            b = a + h*INV_SQ_GOLD_RATIO;
            grad_desc(nn, nn2, grad, b);
            evaluate(nn2, f, sigma_tab, set, &fb, &tpr);
        } else {
            h = d - b;
            a = b; fa = fb;
            b = c; fb = fc;
            c = a + h*INV_GOLD_RATIO;
            grad_desc(nn, nn2, grad, c);
            evaluate(nn2, f, sigma_tab, set, &fc, &tpr);
        }
        //printf("int: [%g, %g]\n", a, d);
    }

    return (a + d)/2.0;

}

