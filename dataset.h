#ifndef DATASET_INCLUDE
#define DATASET_INCLUDE

    #include "load_MNIST.h"


    // insted of 1 use 0.9
    #define ONE 0.9
    // insted of 0 use 0.1
    #define ZERO 0.1

    // get p-th input from dataset
    #define GET_INPUT(set, p) (set->in[set->idcs[p]])
    // get p-th output from dataset
    #define GET_OUTPUT(set, p) (set->out[set->idcs[p]])


    // size: number of examples in dataset
    // idcs: permutation
    // in(inputs), out(outputs)
    typedef struct dataset {
        unsigned int size, *idcs;
        double *a_in, **in;
        double *a_out, **out;
    } dataset;

    // allocate MNIST dataset from ubyte file
    dataset *alloc_MNIST_dataset(char*, char*);
    dataset *balanced_subset_class_dataset(dataset*, unsigned int);
    dataset *balanced_split_class_dataset(dataset*, dataset**, dataset**, double);
    void free_dataset(dataset*);

    // random permute dataset->idcs
    void shuffle(dataset*);

    void print_class_dataset(dataset*);

#endif