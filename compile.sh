#!/bin/sh

gcc load_MNIST.c dataset.c q_dataset.c mul_perceptron.c optimizer.c q_mul_perceptron.c q_optimizer.c utils.c fixed_point.c test.c main.c -O2 -lm -o main
