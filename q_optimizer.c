#include "q_optimizer.h"


void q_grad_desc(q_mlp *q_nn, q_mlp *q_nn2, q_t_bwd *q_grad, long q_learning_rate) {
    unsigned int i;
    for(i = 1; i < q_nn->size; i++) {
        unsigned int j;
        for(j = 0; j < q_nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < q_nn->layers[i-1]; k++) {
                q_nn2->weights[i-1][j][k] = q_sub(q_nn->weights[i-1][j][k], q_mul(q_learning_rate, q_grad->weights[i-1][j][k]));
            }
            q_nn2->biases[i-1][j] = q_sub(q_nn->biases[i-1][j], q_mul(q_learning_rate, q_grad->biases[i-1][j]));
        }
    }
}

void q_adam(q_mlp *q_nn, q_t_bwd *q_grad, q_t_bwd *q_m, q_t_bwd *q_v, long q_learning_rate, long q_b1, long q_b2, long q_pow_b1, long q_pow_b2) {
    unsigned int i;
    long one = 1L << fraction_bits;
    long a = q_mul(q_learning_rate, q_div(q_sqrt(q_sub(one, q_pow_b2)), q_sub(one, q_pow_b1)));
    for(i = 1; i < q_nn->size; i++) {
        unsigned int j;
        for(j = 0; j < q_nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < q_nn->layers[i-1]; k++) {
                q_m->weights[i-1][j][k] = q_add(q_mul(q_b1, q_m->weights[i-1][j][k]), q_mul(q_sub(one, q_b1), q_grad->weights[i-1][j][k]));
                q_v->weights[i-1][j][k] = q_add(q_mul(q_b2, q_v->weights[i-1][j][k]), q_mul(q_sub(one, q_b2), q_mul(q_grad->weights[i-1][j][k], q_grad->weights[i-1][j][k])));
                q_nn->weights[i-1][j][k] = q_sub(q_nn->weights[i-1][j][k], q_mul(a, q_div(q_m->weights[i-1][j][k], q_add(q_sqrt(q_v->weights[i-1][j][k]), 1L))));
            }
            q_m->biases[i-1][j] = q_add(q_mul(q_b1, q_m->biases[i-1][j]), q_mul(q_sub(one, q_b1), q_grad->biases[i-1][j]));
            q_v->biases[i-1][j] = q_add(q_mul(q_b2, q_v->biases[i-1][j]), q_mul(q_sub(one, q_b2), q_mul(q_grad->biases[i-1][j], q_grad->biases[i-1][j])));
            q_nn->biases[i-1][j] = q_sub(q_nn->biases[i-1][j], q_mul(a, q_div(q_m->biases[i-1][j], q_add(q_sqrt(q_v->biases[i-1][j]), 1L))));
        }
    }
}

