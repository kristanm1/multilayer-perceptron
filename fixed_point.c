#include "fixed_point.h"


void q_set_num_bits(int ib, int fb) {
    integer_bits = ib;
    fraction_bits = fb;
    sat_min = -1L << (integer_bits + fraction_bits - 1);
    sat_max = ~sat_min;
}

long q_add(long a, long b) {
    long r = a + b;
    if(r > sat_max) {
        return sat_max;
    } else if(r < sat_min) {
        return sat_min;
    }
    return r;
}

long q_sub(long a, long b) {
    long r = a - b;
    if(r > sat_max) {
        return sat_max;
    } else if(r < sat_min) {
        return sat_min;
    }
    return r;
}

long q_mul(long a, long b) {
    long r = (long_ilm(a, b) + (1L << (fraction_bits - 1))) >> fraction_bits;
    if(r > sat_max) {
        return sat_max;
    } else if(r < sat_min) {
        return sat_min;
    }
    return r;
}

long q_div(long a, long b) {
    if(b == 0L) {
        if(a == 0L) {
            return 0L;
        } else if(a > 0L) {
            return sat_max;
        } else {
            return sat_min;
        }
    }
    char sa = 1, sb = 1;
    if(a < 0) {
        sa = -1;
        a = -a;
    }
    if(b < 0) {
        sb = -1;
        b = -b;
    }
    int k = 0;
    long temp = b;
    while((temp >>= 1) > 0L) {
        k++;
    }
    if(k-fraction_bits < 0) {
        a <<= (fraction_bits-k-1);
        b <<= (fraction_bits-k-1);
    } else {
        a >>= (k-fraction_bits+1);
        b >>= (k-fraction_bits+1);
    }
    long x = q_sub(double_to_fixed(48.0/17.0), q_mul(double_to_fixed(32.0/17.0), b));
    long q_one = 1L << fraction_bits;
    x = q_add(x, q_mul(x, q_sub(q_one, q_mul(b, x))));
    x = q_add(x, q_mul(x, q_sub(q_one, q_mul(b, x))));
    x = q_add(x, q_mul(x, q_sub(q_one, q_mul(b, x))));
    x = q_add(x, q_mul(x, q_sub(q_one, q_mul(b, x))));
    return ((long) (sa*sb))*q_mul(a, x);
}

long q_p_div(long a, long b) {
    if(b == 0L) {
        if(a == 0L) {
            return 0L;
        } else if(a > 0L) {
            return sat_max;
        } else {
            return sat_min;
        }
    }
    long r = a << fraction_bits;
    if((r >= 0 && b >= 0) || (r < 0 && b < 0)) {
        r += (b >> 1);
    } else {
        r -= (b >> 1);
    }
    return r/b;
}

long q_es_sqrt(long a) {
    int k = 0;
    long temp = a;
    while((temp >>= 1) > 0L) {
        k++;
    }
    temp = (a | (1L << (k + 1))) >> 1;
    if(k-fraction_bits < 0) {
        temp <<= ((fraction_bits-k+1) >> 1);
    } else {
        temp >>= ((k-fraction_bits+1) >> 1);
    }
    return temp;
}

long q_sqrt(long a) {
    if(a < 0L) {
        printf("WARNING: negative sqrt!\n");
        return 0L;
    } else if(a == 0L) {
        return 0L;
    }
    long x = q_es_sqrt(a);
    x = q_add(x, q_div(a, x)) >> 1;
    x = q_add(x, q_div(a, x)) >> 1;
    x = q_add(x, q_div(a, x)) >> 1;
    x = q_add(x, q_div(a, x)) >> 1;
    return x;
}

double fixed_to_double(long a) {
    return ((double) a) / (1L << fraction_bits);
}

long double_to_fixed(double a) {
    long r = my_round((a * (1L << fraction_bits)));
    if(r > sat_max) {
        return sat_max;
    } else if(r < sat_min) {
        return sat_min;
    }
    return r;
}

