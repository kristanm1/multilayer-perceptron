#include "q_mul_perceptron.h"


/**********************************************************************************************************************************/

q_mlp *alloc_q_mlp(unsigned int size, unsigned int *layers) {
    q_mlp *q_nn = (q_mlp*) malloc(sizeof(q_mlp));
    q_nn->size = size;
    q_nn->layers = (unsigned int*) malloc(sizeof(unsigned int)*size);
    unsigned int i, t_w = 0, t_b = 0;
    q_nn->layers[0] = layers[0];
    for(i = 1; i < size; i++) {
        q_nn->layers[i] = layers[i];
        t_w += layers[i-1]*layers[i];
        t_b += layers[i];
    }
    q_nn->a_w = (long*) malloc(sizeof(long)*(t_w + t_b));
    q_nn->a_wr = (long**) malloc(sizeof(long*)*(t_b + q_nn->size - 1));
    q_nn->weights = (long***) malloc(sizeof(long**)*(q_nn->size - 1));
    q_nn->a_b = q_nn->a_w + t_w;
    q_nn->biases = q_nn->a_wr + t_b;
    t_w = 0;
    t_b = 0;
    for(i = 1; i < q_nn->size; i++) {
        unsigned int j;
        for(j = 0; j < q_nn->layers[i]; j++) {
            q_nn->a_wr[t_b+j] = q_nn->a_w + t_w+j*q_nn->layers[i-1];
        }
        q_nn->weights[i-1] = q_nn->a_wr + t_b;
        q_nn->biases[i-1] = q_nn->a_b + t_b;
        t_w += q_nn->layers[i-1]*q_nn->layers[i];
        t_b += q_nn->layers[i];
    }
    return q_nn;
}

q_mlp *alloc_q_mlp_from_mlp(mlp *nn) {
    q_mlp *q_nn = (q_mlp*) malloc(sizeof(q_mlp));
    q_nn->size = nn->size;
    q_nn->layers = (unsigned int*) malloc(sizeof(unsigned int)*q_nn->size);
    unsigned int i, t_w = 0, t_b = 0;
    q_nn->layers[0] = nn->layers[0];
    for(i = 1; i < q_nn->size; i++) {
        q_nn->layers[i] = nn->layers[i];
        t_w += q_nn->layers[i-1]*q_nn->layers[i];
        t_b += q_nn->layers[i];
    }
    q_nn->a_w = (long*) malloc(sizeof(long)*(t_w + t_b));
    q_nn->a_wr = (long**) malloc(sizeof(long*)*(t_b + q_nn->size - 1));
    q_nn->weights = (long***) malloc(sizeof(long**)*(q_nn->size - 1));
    q_nn->a_b = q_nn->a_w + t_w;
    q_nn->biases = q_nn->a_wr + t_b;
    t_w = 0;
    t_b = 0;
    for(i = 1; i < q_nn->size; i++) {
        unsigned int j;
        for(j = 0; j < q_nn->layers[i]; j++) {
            q_nn->a_wr[t_b+j] = q_nn->a_w + t_w+j*q_nn->layers[i-1];
            unsigned int k;
            for(k = 0; k < q_nn->layers[i-1]; k++) {
                q_nn->a_w[t_w+j*q_nn->layers[i-1]+k] = double_to_fixed(nn->weights[i-1][j][k]);
            }
            q_nn->a_b[t_b+j] = double_to_fixed(nn->biases[i-1][j]);
        }
        q_nn->weights[i-1] = q_nn->a_wr + t_b;
        q_nn->biases[i-1] = q_nn->a_b + t_b;
        t_w += q_nn->layers[i-1]*q_nn->layers[i];
        t_b += q_nn->layers[i];
    }
    return q_nn;
}

void init_q_mlp_from_q_mlp(q_mlp *q_nn2, q_mlp *q_nn) {
    if(q_nn2->size == q_nn->size && q_nn2->layers[0] == q_nn->layers[0]) {
        unsigned int i;
        for(i = 1; i < q_nn->size; i++) {
            if(q_nn2->layers[i] != q_nn->layers[i]) {
                return;
            }
            unsigned int j;
            for(j = 0; j < q_nn->layers[i]; j++) {
                unsigned int k;
                for(k = 0; k < q_nn->layers[i-1]; k++) {
                    q_nn2->weights[i-1][j][k] = q_nn->weights[i-1][j][k];
                }
                q_nn2->biases[i-1][j] = q_nn->biases[i-1][j];
            }
        }
    }
}

void free_q_mlp(q_mlp *q_nn) {
    free(q_nn->layers);
    free(q_nn->weights);
    free(q_nn->a_wr);
    free(q_nn->a_w);
    free(q_nn);
}

void print_q_mlp(q_mlp *q_nn) {
    unsigned int i;
    for(int i = 0; i < q_nn->size; i++) {
        printf("layer %u: %u\n", i, q_nn->layers[i]);
        if(i > 0) {
            unsigned int j;
            for(j = 0; j < q_nn->layers[i]; j++) {
                unsigned int k;
                for(k = 0; k < q_nn->layers[i-1]; k++) {
                    printf("%12.5g ", fixed_to_double(q_nn->weights[i-1][j][k]));
                }
                printf("| %12.5g\n", fixed_to_double(q_nn->biases[i-1][j]));
            }
        }
    }
    printf("\n");
}

void save_q_mlp(q_mlp *q_nn, char *filename) {
    FILE *f = fopen(filename, "wb");
    fprintf(f, "%u\n", q_nn->size);
    fwrite(q_nn->layers, sizeof(unsigned int), q_nn->size, f);
    unsigned int i;
    unsigned int t_w = 0, t_b = 0;
    for(i = 1; i < q_nn->size; i++) {
        t_w += q_nn->layers[i-1]*q_nn->layers[i];
        t_b += q_nn->layers[i];
    }
    fwrite(q_nn->a_w, sizeof(long), t_w, f);
    fwrite(q_nn->a_b, sizeof(long), t_b, f);
    fclose(f);
}

q_mlp *load_alloc_q_mlp(char *filename) {
    FILE *f = fopen(filename, "rb");
    unsigned int size;
    int r;
    r = fscanf(f, "%u\n", &size);
    unsigned int layers[size];
    r = fread(layers, sizeof(unsigned int), size, f);
    unsigned int i, t_w = 0, t_b = 0;
    for(i = 1; i < size; i++) {
        t_w += layers[i-1]*layers[i];
        t_b += layers[i];
    }
    q_mlp *q_nn = alloc_q_mlp(size, layers);
    r = fread(q_nn->a_w, sizeof(long), t_w, f);
    r = fread(q_nn->a_b, sizeof(long), t_b, f);
    fclose(f);
    return q_nn;
}

/**********************************************************************************************************************************/

q_t_fwd *alloc_q_t_fwd(q_mlp *q_nn) {
    q_t_fwd *q_f = (q_t_fwd*) malloc(sizeof(q_t_fwd));
    unsigned int i, t_a = 0;
    for(i = 0; i < q_nn->size; i++) {
        t_a += q_nn->layers[i];
    }
    q_f->z = (long*) malloc(sizeof(long)*(2*t_a - q_nn->layers[0]));
    q_f->a = q_f->z + t_a - q_nn->layers[0];
    q_f->z_activations = (long**) malloc(sizeof(long)*(2*q_nn->size - 1));
    q_f->activations = q_f->z_activations + q_nn->size - 1;
    t_a = 0;
    q_f->activations[0] = q_f->a;
    for(i = 1; i < q_nn->size; i++) {
        q_f->z_activations[i-1] = q_f->z + t_a;
        q_f->activations[i] = q_f->a + t_a + q_nn->layers[0];
        t_a += q_nn->layers[i];
    }
    return q_f;
}

void free_q_t_fwd(q_t_fwd *q_f) {
    free(q_f->z_activations);
    free(q_f->z);
    free(q_f);
}

void print_q_t_fwd(q_mlp *q_nn, q_t_fwd *q_f) {
    int i;
    for(i = 0; i < q_nn->size; i++) {
        int j;
        printf("layer %d -> ", i);
        for(j = 0; j < q_nn->layers[i]; j++) {
            if(i > 0) {
                printf("%12.5g ", fixed_to_double(q_f->z_activations[i-1][j]));
            } else {
                printf("       empty ");
            }
        }
        printf("-> ");
        for(j = 0; j < q_nn->layers[i]; j++) {
            printf("%12.5g ", fixed_to_double(q_f->activations[i][j]));
        }
        printf("\n");
    }
    printf("\n");
}

/**********************************************************************************************************************************/

q_t_bwd *alloc_q_t_bwd(q_mlp *q_nn) {
    q_t_bwd *q_b = (q_t_bwd*) malloc(sizeof(q_t_bwd));
    unsigned int i, t_w = 0, t_b = 0;
    for(i = 1; i < q_nn->size; i++) {
        t_w += q_nn->layers[i-1]*q_nn->layers[i]; // number of weights
        t_b += q_nn->layers[i]; // number of biases
    }
    q_b->a_w = (long*) malloc(sizeof(long)*(t_w + t_b));
    q_b->a_wr = (long**) malloc(sizeof(long*)*(t_b + q_nn->size-1));
    q_b->weights = (long***) malloc(sizeof(long**)*(q_nn->size-1));
    q_b->a_b = q_b->a_w + t_w;
    q_b->biases = q_b->a_wr + t_b;
    t_w = 0;
    t_b = 0;
    for(i = 1; i < q_nn->size; i++) {
        unsigned int j;
        for(j = 0; j < q_nn->layers[i]; j++) {
            q_b->a_wr[t_b+j] = q_b->a_w + t_w + j*q_nn->layers[i-1];
        }
        q_b->weights[i-1] = q_b->a_wr + t_b;
        q_b->biases[i-1] = q_b->a_b + t_b;
        t_w += q_nn->layers[i-1]*q_nn->layers[i];
        t_b += q_nn->layers[i];
    }
    return q_b;
}

void free_q_t_bwd(q_t_bwd *q_b) {
    free(q_b->weights);
    free(q_b->a_wr);
    free(q_b->a_w);
    free(q_b);
}

void print_q_t_bwd(q_mlp *q_nn, q_t_bwd *q_b) {
    unsigned int i;
    for(i = 1; i < q_nn->size; i++) {
        unsigned int j;
        printf("layer %u\n", i);
        for(j = 0; j < q_nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < q_nn->layers[i-1]; k++) {
                printf("%12.5g ", fixed_to_double(q_b->weights[i-1][j][k]));
            }
            printf("| %12.5g\n", fixed_to_double(q_b->biases[i-1][j]));
        }
    }
    printf("\n");
}

void reset_q_t_bwd(q_mlp *q_nn, q_t_bwd *q_b) {
    unsigned int i;
    for(i = 1; i < q_nn->size; i++) {
        unsigned int j;
        for(j = 0; j < q_nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < q_nn->layers[i-1]; k++) {
                q_b->weights[i-1][j][k] = 0L;
            }
            q_b->biases[i-1][j] = 0L;
        }
    }
}

void add_q_t_bwd(q_mlp *q_nn, q_t_bwd *q_b, q_t_bwd *q_b2) {
    unsigned int i;
    for(i = 1; i < q_nn->size; i++) {
        unsigned int j;
        for(j = 0; j < q_nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < q_nn->layers[i-1]; k++) {
                q_b->weights[i-1][j][k] = q_add(q_b->weights[i-1][j][k], q_b2->weights[i-1][j][k]);
            }
            q_b->biases[i-1][j] = q_add(q_b->biases[i-1][j], q_b2->biases[i-1][j]);
        }
    }
}

/**********************************************************************************************************************************/

long *q_forward(q_mlp *q_nn, q_t_fwd *q_f, q_fun_tab *q_sigma_tab, long *q_in) {
    unsigned int i;
    for(i = 0; i < q_nn->layers[0]; i++) {
        q_f->activations[0][i] = q_in[i];
    }
    for(i = 1; i < q_nn->size; i++) {
        //printf("q_forward: i=%d\n", i);
        unsigned int j;
        for(j = 0; j < q_nn->layers[i]; j++) {
            q_f->z_activations[i-1][j] = q_nn->biases[i-1][j];
            unsigned int k;
            for(k = 0; k < q_nn->layers[i-1]; k++) {
                q_f->z_activations[i-1][j] = q_add(q_f->z_activations[i-1][j], q_mul(q_nn->weights[i-1][j][k], q_f->activations[i-1][k]));
            }
            //printf("q_forward: q_sigma(%g)\n", fixed_to_double(q_f->z_activations[i-1][j]));
            q_f->activations[i][j] = f_q_fun_tab(q_f->z_activations[i-1][j], q_sigma_tab);
        }
    }
    return q_f->activations[q_nn->size-1];
}

void q_backprop(q_mlp *q_nn, q_t_fwd *q_f, q_t_bwd *q_b, q_fun_tab *q_sigma_tab, q_fun_tab *q_d_sigma_tab, long *q_in, long *q_out) {
    q_forward(q_nn, q_f, q_sigma_tab, q_in);
    int i;
    unsigned int t_size = q_nn->size;
    for(i = 0; i < q_nn->layers[t_size - 1]; i++) {
        q_b->biases[t_size-2][i] = q_mul(q_sub(q_f->activations[t_size - 1][i], q_out[i]), f_q_fun_tab(q_f->z_activations[t_size - 2][i], q_d_sigma_tab));
        unsigned int j;
        for(j = 0; j < q_nn->layers[t_size - 2]; j++) {
            q_b->weights[t_size-2][i][j] = q_mul(q_b->biases[t_size - 2][i], q_f->activations[t_size - 2][j]);
        }
    }
    for(i = t_size - 3; i >= 0; i--) {
        unsigned int j;
        for(j = 0; j < q_nn->layers[i+1]; j++) {
            long temp = 0L;
            unsigned int k;
            for(k = 0; k < q_nn->layers[i+2]; k++) {
                temp = q_add(temp, q_mul(q_b->biases[i+1][k], q_nn->weights[i+1][k][j]));
            }
            q_b->biases[i][j] = q_mul(f_q_fun_tab(q_f->z_activations[i][j], q_d_sigma_tab), temp);
            for(k = 0; k < q_nn->layers[i]; k++) {
                q_b->weights[i][j][k] = q_mul(q_b->biases[i][j], q_f->activations[i][k]);
            }
        }
    }
}

/**********************************************************************************************************************************/

void q_cost_x(q_mlp *q_nn, q_t_fwd *q_f, q_fun_tab *q_sigma_tab, long *q_in, long *q_out, double *cost, int *tp) {
    int L = q_nn->layers[q_nn->size - 1], out_argmax = 0, a_argmax = 0;
    double cost_t = 0.0, a[L], out[L];
    //printf("q_cost_x: q_forward\n");
    long *q_a = q_forward(q_nn, q_f, q_sigma_tab, q_in);
    double out_max = fixed_to_double(q_out[0]), a_max = fixed_to_double(q_a[0]);
    int i;
    for(i = 0; i < L; i++) {
        //printf("q_cost_x: i=%d\n", i);
        a[i] = fixed_to_double(q_a[i]);
        out[i] = fixed_to_double(q_out[i]);
        double temp = out[i] - a[i];
        cost_t += temp*temp;
        if(out_max < out[i]) {
            out_max = out[i];
            out_argmax = i;
        }
        if(a_max < a[i]) {
            a_max = a[i];
            a_argmax = i;
        }
    }
    *cost = cost_t/2.0;
    *tp = (L == 1) ? (out[0] < 0.5 && a[0] < 0.5) || (out[0] >= 0.5 && a[0] >= 0.5) : out_argmax == a_argmax;
}

void q_evaluate(q_mlp *q_nn, q_t_fwd *q_f, q_fun_tab *q_sigma_tab, q_dataset *q_set, double *cost, double *tpr) {
    double cost_sum = 0.0;
    int i, tp_sum = 0;
    for(i = 0; i < q_set->size; i++) {
        double cost_t;
        int tp_t;
        //printf("q_evaluate: i=%d\n", i);
        q_cost_x(q_nn, q_f, q_sigma_tab, Q_GET_INPUT(q_set, i), Q_GET_OUTPUT(q_set, i), &cost_t, &tp_t);
        cost_sum += cost_t;
        tp_sum += tp_t;
    }
    *cost = cost_sum/q_set->size;
    *tpr = ((double) tp_sum)/q_set->size;
}

/**********************************************************************************************************************************/

void q_init_for_train(q_mlp *q_nn, q_mlp **q_nn2, q_mlp **q_nn_opt, q_t_fwd **q_f, q_t_bwd **q_grad, q_t_bwd **q_b, q_fun_tab **q_sigma_tab, q_fun_tab **q_d_sigma_tab) {
    if(q_nn2) {
        *q_nn2 = alloc_q_mlp(q_nn->size, q_nn->layers);
    }
    *q_nn_opt = alloc_q_mlp(q_nn->size, q_nn->layers);
    *q_f = alloc_q_t_fwd(q_nn);
    *q_grad = alloc_q_t_bwd(q_nn);
    *q_b = alloc_q_t_bwd(q_nn);
    *q_sigma_tab = alloc_q_fun_tab(fun_tab_low, fun_tab_high, fun_tab_size, sigma);
    *q_d_sigma_tab = alloc_q_fun_tab(fun_tab_low, fun_tab_high, fun_tab_size, d_sigma);
}

void q_free_from_train(q_mlp *q_nn, q_mlp *q_nn2, q_mlp *q_nn_opt, q_t_fwd *q_f, q_t_bwd *q_grad, q_t_bwd *q_b, q_fun_tab *q_sigma_tab, q_fun_tab *q_d_sigma_tab) {
    if(q_nn2) {
        free_q_mlp(q_nn2);
    }
    init_q_mlp_from_q_mlp(q_nn, q_nn_opt);
    free_q_mlp(q_nn_opt);
    free_q_t_fwd(q_f);
    free_q_t_bwd(q_grad);
    free_q_t_bwd(q_b);
    free_q_fun_tab(q_sigma_tab);
    free_q_fun_tab(q_d_sigma_tab);
}

void q_batch_derivative(q_mlp *q_nn, q_t_fwd *q_f, q_t_bwd *q_grad, q_t_bwd *q_b, q_fun_tab *q_sigma_tab, q_fun_tab *q_d_sigma_tab, unsigned int batch_size, unsigned int batch_start, q_dataset *q_train_set) {
    reset_q_t_bwd(q_nn, q_grad);
    unsigned int i, batch_end = batch_start + batch_size;
    if(batch_end > q_train_set->size) {
        batch_end = q_train_set->size;
    }
    for(i = batch_start; i < batch_end; i++) {
        q_backprop(q_nn, q_f, q_b, q_sigma_tab, q_d_sigma_tab, Q_GET_INPUT(q_train_set, i), Q_GET_OUTPUT(q_train_set, i));
        add_q_t_bwd(q_nn, q_grad, q_b);
    }
}

/**********************************************************************************************************************************/

void q_batch_grad_desc(q_mlp *q_nn, q_t_fwd *q_f, q_t_bwd *q_grad, q_t_bwd *q_b, q_fun_tab *q_sigma_tab, q_fun_tab *q_d_sigma_tab, unsigned int batch_size, unsigned int batch_start, long q_learning_rate, q_dataset *q_train_set) {
    q_batch_derivative(q_nn, q_f, q_grad, q_b, q_sigma_tab, q_d_sigma_tab, batch_size, batch_start, q_train_set);
    q_grad_desc(q_nn, q_nn, q_grad, q_learning_rate);
}

void q_batch_adam(q_mlp *q_nn, q_t_fwd *q_f, q_t_bwd *q_grad, q_t_bwd *q_b, q_t_bwd *q_m, q_t_bwd *q_v, q_fun_tab *q_sigma_tab, q_fun_tab *q_d_sigma_tab, unsigned int batch_size, unsigned int batch_start, long q_learning_rate, long q_b1, long q_b2, long q_pow_b1, long q_pow_b2, q_dataset *q_traint_set) {
    q_batch_derivative(q_nn, q_f, q_grad, q_b, q_sigma_tab, q_d_sigma_tab, batch_size, batch_start, q_traint_set);
    q_adam(q_nn, q_grad, q_m, q_v, q_learning_rate, q_b1, q_b2, q_pow_b1, q_pow_b2);
}

void q_train_epochs_grad_desc(q_mlp *q_nn, unsigned int epochs, unsigned int batch_size, long q_learning_rate, dataset *set) {
    q_t_fwd *q_f;
    q_t_bwd *q_b, *q_grad;
    q_fun_tab *q_sigma_tab, *q_d_sigma_tab;
    dataset *train_set, *valid_set;
    q_mlp *q_nn_opt;
    q_init_for_train(q_nn, 0, &q_nn_opt, &q_f, &q_grad, &q_b, &q_sigma_tab, &q_d_sigma_tab);
    balanced_split_class_dataset(set, &train_set, &valid_set, 0.8);
    q_dataset *q_train_set = alloc_q_dataset_from_dataset(train_set);
    q_dataset *q_valid_set = alloc_q_dataset_from_dataset(valid_set);
    free_dataset(train_set); free_dataset(valid_set);
    printf("FIXED POINT:\n");
    printf("range: [%g, %g]\n", fixed_to_double(sat_min), fixed_to_double(sat_max));
    printf("precision %g\n", fixed_to_double(1L));
    printf("q_sigma_tab->step: %g\n\n", fixed_to_double(q_sigma_tab->step));
    double cost, tpr;
    clock_t time = clock();
    q_evaluate(q_nn, q_f, q_sigma_tab, q_valid_set, &cost, &tpr);
    printf("epochs: %d\n", epochs);
    printf("batch size: %d\n", batch_size);
    printf("learning rate: %.5f\n", fixed_to_double(q_learning_rate));
    printf("integer bits: %d fractional bits: %d\n", integer_bits, fraction_bits);
    printf("|  epoch |  cost(MSE) | TPR(%%) | time(sec) |\n");
    printf("|   init | %10.6f | %6.4f | %9.2f |\n", cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    double cost_opt = cost;
    unsigned int epoch;
    for(epoch = 0; epoch < epochs; epoch++) {
        time = clock();
        q_shuffle(q_train_set);
        unsigned int i = 0;
        for(i = 0; i < q_train_set->size; i += batch_size) {
            q_batch_grad_desc(q_nn, q_f, q_grad, q_b, q_sigma_tab, q_d_sigma_tab, batch_size, i, q_learning_rate, q_train_set);
        }
        q_evaluate(q_nn, q_f, q_sigma_tab, q_valid_set, &cost, &tpr);
        if(cost < cost_opt) {
            cost_opt = cost;
            init_q_mlp_from_q_mlp(q_nn_opt, q_nn);
        }
        printf("|%6d  | %10.6f | %6.4f | %9.2f |\n", epoch, cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    }
    q_free_from_train(q_nn, 0, q_nn_opt, q_f, q_grad, q_b, q_sigma_tab, q_d_sigma_tab);
}

void q_train_epochs_adam(q_mlp *q_nn, unsigned int epochs, unsigned int batch_size, long q_learning_rate, long q_b1, long q_b2, dataset *set) {
    q_t_fwd *q_f;
    q_t_bwd *q_b, *q_grad, *q_m = alloc_q_t_bwd(q_nn), *q_v = alloc_q_t_bwd(q_nn);
    reset_q_t_bwd(q_nn, q_m); reset_q_t_bwd(q_nn, q_v);
    q_fun_tab *q_sigma_tab, *q_d_sigma_tab;
    dataset *train_set, *valid_set;
    q_mlp *q_nn_opt;
    q_init_for_train(q_nn, 0, &q_nn_opt, &q_f, &q_grad, &q_b, &q_sigma_tab, &q_d_sigma_tab);
    balanced_split_class_dataset(set, &train_set, &valid_set, 0.8);
    q_dataset *q_train_set = alloc_q_dataset_from_dataset(train_set);
    q_dataset *q_valid_set = alloc_q_dataset_from_dataset(valid_set);
    free_dataset(train_set); free_dataset(valid_set);
    printf("FIXED POINT:\n");
    printf("range: [%g, %g]\n", fixed_to_double(sat_min), fixed_to_double(sat_max));
    printf("precision %g\n", fixed_to_double(1L));
    printf("q_sigma_tab->step: %g\n\n", fixed_to_double(q_sigma_tab->step));
    double cost, tpr;
    clock_t time = clock();
    q_evaluate(q_nn, q_f, q_sigma_tab, q_valid_set, &cost, &tpr);
    printf("epochs: %d\n", epochs);
    printf("batch size: %d\n", batch_size);
    printf("learning rate: %.5f\n", fixed_to_double(q_learning_rate));
    printf("integer bits: %d fractional bits: %d\n", integer_bits, fraction_bits);
    printf("|  epoch |  cost(MSE) | TPR(%%) | time(sec) |\n");
    printf("|   init | %10.6f | %6.4f | %9.2f |\n", cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    double cost_opt = cost;
    long q_pow_b1 = q_b1, q_pow_b2 = q_b2;
    unsigned int epoch;
    for(epoch = 0; epoch < epochs; epoch++) {
        time = clock();
        q_shuffle(q_train_set);
        unsigned int i = 0;
        for(i = 0; i < q_train_set->size; i += batch_size) {
            q_batch_adam(q_nn, q_f, q_grad, q_b, q_m, q_v, q_sigma_tab, q_d_sigma_tab, batch_size, i, q_learning_rate, q_b1, q_b2, q_pow_b1, q_pow_b2, q_train_set);
        }
        q_pow_b1 = q_mul(q_pow_b1, q_b1);
        q_pow_b2 = q_mul(q_pow_b2, q_b2);
        q_evaluate(q_nn, q_f, q_sigma_tab, q_valid_set, &cost, &tpr);
        if(cost < cost_opt) {
            cost_opt = cost;
            init_q_mlp_from_q_mlp(q_nn_opt, q_nn);
        }
        printf("|%6d  | %10.6f | %6.4f | %9.2f |\n", epoch, cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    }
    q_free_from_train(q_nn, 0, q_nn_opt, q_f, q_grad, q_b, q_sigma_tab, q_d_sigma_tab);
    free_q_t_bwd(q_m); free_q_t_bwd(q_v);
    free_q_dataset(q_train_set); free_q_dataset(q_valid_set);
}

void q_train_grad_desc(q_mlp *q_nn, unsigned int batch_size, long q_learning_rate, dataset *set) {
    q_t_fwd *q_f;
    q_t_bwd *q_b, *q_grad;
    q_fun_tab *q_sigma_tab, *q_d_sigma_tab;
    dataset *train_set, *valid_set;
    q_mlp *q_nn_opt;
    q_init_for_train(q_nn, 0, &q_nn_opt, &q_f, &q_grad, &q_b, &q_sigma_tab, &q_d_sigma_tab);
    balanced_split_class_dataset(set, &train_set, &valid_set, 0.8);
    q_dataset *q_train_set = alloc_q_dataset_from_dataset(train_set);
    q_dataset *q_valid_set = alloc_q_dataset_from_dataset(valid_set);
    free_dataset(train_set); free_dataset(valid_set);

    printf("FIXED POINT:\n");
    printf("range: [%g, %g]\n", fixed_to_double(sat_min), fixed_to_double(sat_max));
    printf("precision %g\n\n", fixed_to_double(1L));

    printf("TABULATED FUNCTIONS:\n");
    printf("range: [%g, %g]\n", fixed_to_double(q_sigma_tab->start), fixed_to_double(q_sigma_tab->end));
    printf("step: %g\n\n", fixed_to_double(q_sigma_tab->step));

    double cost, tpr;
    clock_t time = clock();
    //printf("\nq_train\n");
    q_evaluate(q_nn, q_f, q_sigma_tab, q_valid_set, &cost, &tpr);
    printf("batch size: %d\n", batch_size);
    printf("learning rate: %.5f\n", fixed_to_double(q_learning_rate));
    printf("integer bits: %d fractional bits: %d\n", integer_bits, fraction_bits);
    printf("|  epoch |  cost(MSE) | TPR(%%) | time(sec) |\n");
    printf("|   init | %10.6f | %6.4f | %9.2f |\n", cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);

    double cost_opt = cost;
    unsigned int epoch = 0, epoch_opt = 0, epoch_stop = 1;
    while(1) {
        time = clock();
        q_shuffle(q_train_set);

        unsigned int i = 0;
        for(i = 0; i < q_train_set->size; i += batch_size) {
            q_batch_grad_desc(q_nn, q_f, q_grad, q_b, q_sigma_tab, q_d_sigma_tab, batch_size, i, q_learning_rate, q_train_set);
        }
        q_evaluate(q_nn, q_f, q_sigma_tab, q_valid_set, &cost, &tpr);
        printf("|%6d  | %10.6f | %6.4f | %9.2f |\n", epoch, cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
        if(cost < cost_opt) {
            cost_opt = cost;
            epoch_opt = epoch;
            epoch_stop = (unsigned int) (1.1*epoch + 1);
            init_q_mlp_from_q_mlp(q_nn_opt, q_nn);
        }
        if(epoch == epoch_stop) {
            break;
        }
        epoch++;
    }
    q_free_from_train(q_nn, 0, q_nn_opt, q_f, q_grad, q_b, q_sigma_tab, q_d_sigma_tab);
}

void q_train_adam(q_mlp *q_nn, unsigned int batch_size, long q_learning_rate, long q_b1, long q_b2, dataset *set) {
    q_t_fwd *q_f;
    q_t_bwd *q_b, *q_grad, *q_m = alloc_q_t_bwd(q_nn), *q_v = alloc_q_t_bwd(q_nn);
    reset_q_t_bwd(q_nn, q_m); reset_q_t_bwd(q_nn, q_v);
    q_fun_tab *q_sigma_tab, *q_d_sigma_tab;
    dataset *train_set, *valid_set;
    q_mlp *q_nn_opt;
    q_init_for_train(q_nn, 0, &q_nn_opt, &q_f, &q_grad, &q_b, &q_sigma_tab, &q_d_sigma_tab);
    balanced_split_class_dataset(set, &train_set, &valid_set, 0.8);
    q_dataset *q_train_set = alloc_q_dataset_from_dataset(train_set);
    q_dataset *q_valid_set = alloc_q_dataset_from_dataset(valid_set);
    free_dataset(train_set); free_dataset(valid_set);
    printf("FIXED POINT:\n");
    printf("range: [%g, %g]\n", fixed_to_double(sat_min), fixed_to_double(sat_max));
    printf("precision %g\n", fixed_to_double(1L));
    printf("q_sigma_tab->step: %g\n\n", fixed_to_double(q_sigma_tab->step));
    double cost, tpr;
    clock_t time = clock();
    q_evaluate(q_nn, q_f, q_sigma_tab, q_valid_set, &cost, &tpr);
    printf("batch size: %d\n", batch_size);
    printf("learning rate: %.5f\n", fixed_to_double(q_learning_rate));
    printf("integer bits: %d fractional bits: %d\n", integer_bits, fraction_bits);
    printf("|  epoch |  cost(MSE) | TPR(%%) | time(sec) |\n");
    printf("|   init | %10.6f | %6.4f | %9.2f |\n", cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    long q_pow_b1 = q_b1, q_pow_b2 = q_b2;
    double cost_opt = cost;
    unsigned int epoch = 0, epoch_opt = 0, epoch_stop = 1;
    while(1) {
        time = clock();
        q_shuffle(q_train_set);
        unsigned int i = 0;
        for(i = 0; i < q_train_set->size; i += batch_size) {
            q_batch_adam(q_nn, q_f, q_grad, q_b, q_m, q_v, q_sigma_tab, q_d_sigma_tab, batch_size, i, q_learning_rate, q_b1, q_b2, q_pow_b1, q_pow_b2, q_train_set);
        }
        q_pow_b1 = q_mul(q_pow_b1, q_b1);
        q_pow_b2 = q_mul(q_pow_b2, q_b2);
        q_evaluate(q_nn, q_f, q_sigma_tab, q_valid_set, &cost, &tpr);
        printf("|%6d  | %10.6f | %6.4f | %9.2f |\n", epoch, cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
        if(cost < cost_opt) {
            cost_opt = cost;
            epoch_opt = epoch;
            epoch_stop = (unsigned int) (1.1*epoch + 1);
            init_q_mlp_from_q_mlp(q_nn_opt, q_nn);
        }
        if(epoch == epoch_stop) {
            break;
        }
        epoch++;
    }
    q_free_from_train(q_nn, 0, q_nn_opt, q_f, q_grad, q_b, q_sigma_tab, q_d_sigma_tab);
    free_q_t_bwd(q_m); free_q_t_bwd(q_v);
    free_q_dataset(q_train_set); free_q_dataset(q_valid_set);
}
