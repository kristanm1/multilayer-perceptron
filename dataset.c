#include "dataset.h"


dataset *alloc_MNIST_dataset(char *labs_filename, char *imgs_filename) {
    dataset *set = (dataset*) malloc(sizeof(dataset));
    labels *labs = load_labels(labs_filename);
    set->size = labs->size;
    set->idcs = (unsigned int*) malloc(sizeof(unsigned int)*set->size);
    set->a_out = (double*) malloc(sizeof(double)*set->size*10);
    set->out = (double**) malloc(sizeof(double*)*set->size);
    unsigned int i;
    for(i = 0; i < set->size; i++) {
        set->idcs[i] = i;
        unsigned int j, out_start = i*10, label = (unsigned int) labs->data[i];
        set->out[i] = &set->a_out[out_start];
        for(j = 0; j < 10; j++) {
            if(j == label) {
                set->a_out[out_start+j] = ONE;
            } else {
                set->a_out[out_start+j] = ZERO;
            }
        }
    }
    free_labels(labs);
    images *imgs = load_images(imgs_filename);
    unsigned int img_size = imgs->height*imgs->width;
    set->a_in = (double*) malloc(sizeof(double)*set->size*img_size);
    set->in = (double**) malloc(sizeof(double*)*set->size);
    for(i = 0; i < set->size; i++) {
        unsigned int j, in_start = i*img_size;
        set->in[i] = &set->a_in[in_start];
        for(j = 0; j < img_size; j++) {
            set->a_in[in_start+j] = ((double) imgs->data[in_start+j]) / 255.0;
        }
    }
    free_images(imgs);
    return set;
}

dataset *balanced_subset_class_dataset(dataset *set, unsigned int size) {
    unsigned int in_size = set->in[1] - set->in[0];
    unsigned int out_size = set->out[1] - set->out[0];
    dataset *subset = (dataset*) malloc(sizeof(dataset));
    subset->size = size;
    subset->idcs = (unsigned int*) malloc(sizeof(unsigned int)*size);
    subset->a_in = (double*) malloc(sizeof(double)*size*in_size);
    subset->in = (double**) malloc(sizeof(double*)*size);
    subset->a_out = (double*) malloc(sizeof(double)*size*out_size);
    subset->out = (double**) malloc(sizeof(double*)*size);
    unsigned int i, count[out_size], j = size%out_size, p = size/out_size;
    for(i = 0; i < out_size; i++) {
        count[i] = (j > 0 && i < j) ? p + 1 : p;
    }
    p = 0;
    for(i = 0; i < set->size; i++) {
        double *out = GET_OUTPUT(set, i);
        unsigned int c = 0;
        for(j = 1; j < out_size; j++) {
            if(out[c] < out[j]) {
                c = j;
            }
        }
        if(count[c] > 0) {
            subset->idcs[p] = p;
            subset->in[p] = subset->a_in + in_size*p;
            subset->out[p] = subset->a_out + out_size*p;
            double *in = GET_INPUT(set, i);
            for(j = 0; j < in_size; j++) {
                subset->in[p][j] = in[j];
            }
            for(j = 0; j < out_size; j++) {
                subset->out[p][j] = out[j];
            }
            p++;
            count[c]--;
        }
    }
    return subset;
}

dataset *balanced_split_class_dataset(dataset *set, dataset **subset1, dataset **subset2, double ratio) {

    dataset *subset;
    unsigned int in_size = set->in[1] - set->in[0];
    unsigned int out_size = set->out[1] - set->out[0];

    subset = (dataset*) malloc(sizeof(dataset));
    subset->size = (unsigned int) ((set->size * ratio) + 0.5);
    subset->idcs = (unsigned int*) malloc(sizeof(unsigned int)*subset->size);
    subset->a_in = (double*) malloc(sizeof(double)*subset->size*in_size);
    subset->in = (double**) malloc(sizeof(double*)*subset->size);
    subset->a_out = (double*) malloc(sizeof(double)*subset->size*out_size);
    subset->out = (double**) malloc(sizeof(double*)*subset->size);
    unsigned int i, count[out_size], j = subset->size%out_size, p = subset->size/out_size, q;
    for(i = 0; i < out_size; i++) {
        count[i] = (j > 0 && i < j) ? p + 1 : p;
    }
    *subset1 = subset;

    subset = (dataset*) malloc(sizeof(dataset));
    subset->size = set->size - (*subset1)->size;
    subset->idcs = (unsigned int*) malloc(sizeof(unsigned int)*subset->size);
    subset->a_in = (double*) malloc(sizeof(double)*subset->size*in_size);
    subset->in = (double**) malloc(sizeof(double*)*subset->size);
    subset->a_out = (double*) malloc(sizeof(double)*subset->size*out_size);
    subset->out = (double**) malloc(sizeof(double*)*subset->size);
    *subset2 = subset;

    p = 0;
    q = 0;
    for(i = 0; i < set->size; i++) {
        double *out = GET_OUTPUT(set, i);
        unsigned int j, c = 0;
        for(j = 1; j < out_size; j++) {
            if(out[c] < out[j]) {
                c = j;
            }
        }
        double *in = GET_INPUT(set, i);
        if(count[c] > 0 || (*subset1)->size == set->size) {
            (*subset1)->idcs[p] = p;
            (*subset1)->in[p] = (*subset1)->a_in + in_size*p;
            (*subset1)->out[p] = (*subset1)->a_out + out_size*p;
            for(j = 0; j < in_size; j++) {
                (*subset1)->in[p][j] = in[j];
            }
            for(j = 0; j < out_size; j++) {
                (*subset1)->out[p][j] = out[j];
            }
            p++;
            count[c]--;
        }
        else {
            (*subset2)->idcs[q] = q;
            (*subset2)->in[q] = (*subset2)->a_in + in_size*q;
            (*subset2)->out[q] = (*subset2)->a_out + out_size*q;
            for(j = 0; j < in_size; j++) {
                (*subset2)->in[q][j] = in[j];
            }
            for(j = 0; j < out_size; j++) {
                (*subset2)->out[q][j] = out[j];
            }
            q++;
        }
    }

}

void free_dataset(dataset *set) {
    free(set->idcs);
    free(set->in);
    free(set->a_in);
    free(set->out);
    free(set->a_out);
    free(set);
}

/**********************************************************************************************************************************/

void shuffle(dataset *set) {
    unsigned int i;
    for(i = set->size; i > 1; i--) {
        unsigned int r = (unsigned int) (rand()%i);
        if(r < i-1) {
            unsigned int temp = set->idcs[i-1];
            set->idcs[i-1] = set->idcs[r];
            set->idcs[r] = temp;
        }
    }
}

/**********************************************************************************************************************************/

void print_class_dataset(dataset *set) {
    if(set->size > 1) {
        unsigned int in_size = set->in[1] - set->in[0];
        unsigned int out_size = set->out[1] - set->out[0];
        unsigned int i, count[out_size];
        for(i = 0; i < out_size; i++) {
            count[i] = 0;
        }
        for(i = 0; i < set->size; i++) {
            unsigned int j, max = 0;
            double *out = GET_OUTPUT(set, i);
            for(j = 1; j < out_size; j++) {
                if(out[max] < out[j]) {
                    max = j;
                }
            }
            count[max]++;
        }
        printf("set->size = %u\n", set->size);
        printf(" in|%u -> out|%u\n", in_size, out_size);
        for(i = 0; i < out_size; i++) {
            printf("%2u -> %u\n", i, count[i]);
        }
    }
}
