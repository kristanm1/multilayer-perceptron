#ifndef Q_OPTIMIZER_INCLUDE
#define Q_OPTIMIZER_INCLUDE

    #include "q_mul_perceptron.h"


    typedef struct q_mlp q_mlp;
    typedef struct q_t_bwd q_t_bwd;
    typedef struct q_t_fwd q_t_fwd;
    typedef struct q_t_bwd q_t_bwd;

// MULTIVARIABLE OPTIMIZATION

    void q_grad_desc(q_mlp*, q_mlp*, q_t_bwd*, long);
    void q_adam(q_mlp*, q_t_bwd*, q_t_bwd*, q_t_bwd*, long, long, long, long, long);

#endif