#ifndef TEST_INCLUDE
#define TEST_INCLUDE

    #include <time.h>

    #include "dataset.h"
    #include "q_dataset.h"
    #include "mul_perceptron.h"
    #include "q_mul_perceptron.h"
    #include "fixed_point.h"


    extern int bits_cut, integer_bits, fraction_bits;
    extern long sat_min, sat_max;
    extern double fun_tab_low, fun_tab_high;
    extern unsigned int fun_tab_size;

    // backpropagation example
    // https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/
    dataset *alloc_matmazur_dataset(); // OK
    void test_mattmazur(); // OK

    // train mlp (layers=[2, 2, 1]) xor
    dataset *alloc_XOR_dataset(); // OK
    void test_xor(int, char**); // OK
    // train q_mlp (layers=[2, 2, 1]) xor
    void test_q_xor(int, char**);

    // allocates simple 2x2 pixel images as inputs
    // output is diagonal, horizontal or vertical
    // 0 1  and  1 0  are diagonals |  0 1  and  0 1  are horizontals |  1 1  and  0 0  are verticals
    // 1 0       0 1  -> [1, 0, 0]  |  0 1  and  0 1  -> [0, 1, 0]    |  0 0       1 1  -> [0, 0, 1]
    dataset *alloc_simple_dataset(); // OK

    // allocate dataset with (size) random examples
    // in-s are in_size width and out-s are out_size width: all elements are random set from (-1, 1)
    dataset *alloc_random_dataset(unsigned int, unsigned int, unsigned int);

    // test MACRO: CUT_BITS_double
    void test_rounder(); // OK

    // create subset of MNIST train set and save p-th example from subset to disk as test_%u.pmg
    void test_dataset(); // OK
    void test_datasubset();
    // create fixed point subset of MNIST train set and save p-th example from subset to disk as test_%u.pmg
    void test_q_dataset(); // OK
    // compare floating point and fixed point dataset
    void test_MNIST_dataset_and_q_dataset();

    // calculate mean and sd of z_activations for all neurons in forward passes
    void test_avg_std(); // OK
    void test_MNIST_avg_std(); // OK

    // test fixed point aritmetics
    void test_fixed_point(); // OK

    // test tabulated function f(x) = e^x
    void test_fun_tab(); // OK

    // compare floating point and fixed point mlp
    void test_mattmazur_mlp_and_q_mlp(); // OK
    void test_random_mlp_and_q_mlp(); // OK
    void test_batch_derivative(); // OK

    // test online mean and variance calculation
    void test_moving_average(); // OK

    void test_multipliers(); // OK

    void test_gss();


#endif