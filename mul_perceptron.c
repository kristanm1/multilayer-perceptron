#include "mul_perceptron.h"


double sigma(double x) {
    return 1 / (1 + exp(-x));
}

double d_sigma(double x) {
    double y = sigma(x);
    return y * (1 - y);
}

/**********************************************************************************************************************************/

mlp *alloc_mlp(unsigned int size, unsigned int *layers) {
    mlp *nn = (mlp*) malloc(sizeof(mlp));
    nn->size = size;
    nn->layers = (unsigned int*) malloc(sizeof(unsigned int)*size);
    unsigned int i, t_w = 0, t_b = 0;
    nn->layers[0] = layers[0];
    for(i = 1; i < size; i++) {
        nn->layers[i] = layers[i];
        t_w += layers[i-1]*layers[i]; // number of weights
        t_b += layers[i]; // number of biases
    }
    nn->a_w = (double*) malloc(sizeof(double)*(t_w + t_b)); // allocate memory for weights and biases
    nn->a_b = nn->a_w + t_w;
    nn->a_wr = (double**) malloc(sizeof(double*)*(t_b + size-1));
    nn->biases = nn->a_wr + t_b;
    nn->weights = (double***) malloc(sizeof(double**)*(size-1));
    nn->a_w_means = (double*) malloc(sizeof(double)*4*(t_w + t_b)); // allocate memory for statistics
    nn->a_w_sd = nn->a_w_means + t_w;
    nn->a_w_min = nn->a_w_sd + t_w;
    nn->a_w_max = nn->a_w_min + t_w;
    nn->a_b_means = nn->a_w_max + t_w;
    nn->a_b_sd = nn->a_b_means + t_b;
    nn->a_b_min = nn->a_b_sd + t_b;
    nn->a_b_max = nn->a_b_min + t_b;
    nn->a_wr_means = (double**) malloc(sizeof(double**)*4*(t_b + size - 1));
    nn->a_wr_sd = nn->a_wr_means + t_b;
    nn->a_wr_min = nn->a_wr_sd + t_b;
    nn->a_wr_max = nn->a_wr_min + t_b;
    nn->b_means = nn->a_wr_max + t_b;
    nn->b_sd = nn->b_means + size - 1;
    nn->b_min = nn->b_sd + size - 1;
    nn->b_max = nn->b_min + size - 1;
    nn->w_means = (double***) malloc(sizeof(double**)*4*(size - 1));
    nn->w_sd = nn->w_means + size - 1;
    nn->w_min = nn->w_sd + size - 1;
    nn->w_max = nn->w_min + size - 1;
    t_w = 0;
    t_b = 0;
    for(i = 1; i < size; i++) {
        unsigned int j;
        for(j = 0; j < layers[i]; j++) {
            nn->a_wr[t_b+j] = nn->a_w + t_w + j*layers[i-1];
            nn->a_wr_means[t_b+j] = nn->a_w_means + t_w + j*layers[i-1];
            nn->a_wr_sd[t_b+j] = nn->a_w_sd + t_w + j*layers[i-1];
            nn->a_wr_min[t_b+j] = nn->a_w_min + t_w + j*layers[i-1];
            nn->a_wr_max[t_b+j] = nn->a_w_max + t_w + j*layers[i-1];
        }
        nn->weights[i-1] = nn->a_wr + t_b;
        nn->biases[i-1] = nn->a_b + t_b;
        nn->w_means[i-1] = nn->a_wr_means + t_b;
        nn->w_sd[i-1] = nn->a_wr_sd + t_b;
        nn->w_min[i-1] = nn->a_wr_min + t_b;
        nn->w_max[i-1] = nn->a_wr_max + t_b;
        nn->b_means[i-1] = nn->a_b_means + t_b;
        nn->b_sd[i-1] = nn->a_b_sd + t_b;
        nn->b_min[i-1] = nn->a_b_min + t_b;
        nn->b_max[i-1] = nn->a_b_max + t_b;
        t_w += layers[i-1]*layers[i];
        t_b += layers[i];
    }
    return nn;
}

void free_mlp(mlp *nn) {
    free(nn->layers);
    free(nn->weights);
    free(nn->a_wr);
    free(nn->a_w);
    free(nn->a_w_means);
    free(nn->a_wr_means);
    free(nn->w_means);
    free(nn);
}

void init_rand_mlp(mlp *nn, double fact) {
    unsigned int i;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                nn->weights[i-1][j][k] = fact*(2*(rand()/((double) RAND_MAX)) - 1);
            }
            nn->biases[i-1][j] = fact*(2*(rand()/((double) RAND_MAX)) - 1);
        }
    }
    add_stat_mlp(nn);
}

void init_mlp_from_mlp(mlp *nn2, mlp *nn) {
    if(nn->size == nn2->size && nn->layers[0] == nn2->layers[0]) {
        unsigned int i;
        for(i = 1; i < nn->size; i++) {
            if(nn->layers[i] != nn2->layers[i]) {
                return;
            }
            unsigned int j;
            for(j = 0; j < nn->layers[i]; j++) {
                unsigned int k;
                for(k = 0; k < nn->layers[i-1]; k++) {
                    nn2->weights[i-1][j][k] = nn->weights[i-1][j][k];
                }
                nn2->biases[i-1][j] = nn->biases[i-1][j];
            }
        }
    }
}

void print_mlp(mlp *nn) {
    unsigned int i;
    for(int i = 0; i < nn->size; i++) {
        printf("layer %u: %u\n", i, nn->layers[i]);
        if(i > 0) {
            unsigned int j;
            for(j = 0; j < nn->layers[i]; j++) {
                unsigned int k;
                for(k = 0; k < nn->layers[i-1]; k++) {
                    printf("%12.5g ", nn->weights[i-1][j][k]);
                }
                printf("| %12.5g\n", nn->biases[i-1][j]);
            }
        }
    }
    printf("\n");
}

void reset_stat_mlp(mlp *nn) {
    unsigned int i;
    long inf = (0x7ffL << 52), ninf = (0xfffL << 52);
    nn->i = 0;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                nn->w_means[i-1][j][k] = 0.0;
                nn->w_sd[i-1][j][k] = 0.0;
                nn->w_min[i-1][j][k] = *((double*) &inf);
                nn->w_max[i-1][j][k] = *((double*) &ninf);
            }
            nn->b_means[i-1][j] = 0.0;
            nn->b_sd[i-1][j] = 0.0;
            nn->b_min[i-1][j] = *((double*) &inf);
            nn->b_max[i-1][j] = *((double*) &ninf);
        }
    }
}

void print_stat_mlp(mlp *nn) {
    unsigned int i;
    printf(".:MLP:.            mean    |      sd      |     min.     |     max.\n");
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        printf(" layer [%3d] :---------------------------------------------------------:\n", i-1);
        for(j = 0; j < nn->layers[i]; j++) {
            printf("neuron [%3d] :             :              :              :             :\n", j);
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                printf("   w %3d     [%12.4e | %12.4e | %12.4e | %12.4e]\n", k, nn->w_means[i-1][j][k], sqrt(nn->w_sd[i-1][j][k]/nn->i), nn->w_min[i-1][j][k], nn->w_max[i-1][j][k]);
            }
            printf("  *b         [%12.4e | %12.4e | %12.4e | %12.4e]\n", nn->b_means[i-1][j], sqrt(nn->b_sd[i-1][j]/nn->i), nn->b_min[i-1][j], nn->b_max[i-1][j]);
        }
    }
    printf("\n");
}

void add_stat_mlp(mlp *nn) {
    unsigned int i;
    double temp;
    nn->i++;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                temp = nn->w_means[i-1][j][k] + (nn->weights[i-1][j][k] - nn->w_means[i-1][j][k])/nn->i;
                nn->w_sd[i-1][j][k] = nn->w_sd[i-1][j][k] + (nn->weights[i-1][j][k] - nn->w_means[i-1][j][k])*(nn->weights[i-1][j][k] - temp);
                nn->w_means[i-1][j][k] = temp;
                if(nn->weights[i-1][j][k] < nn->w_min[i-1][j][k]) {
                    nn->w_min[i-1][j][k] = nn->weights[i-1][j][k];
                }
                if(nn->w_max[i-1][j][k] < nn->weights[i-1][j][k]) {
                    nn->w_max[i-1][j][k] = nn->weights[i-1][j][k];
                }
            }
            temp = nn->b_means[i-1][j] + (nn->biases[i-1][j] - nn->b_means[i-1][j])/nn->i;
            nn->b_sd[i-1][j] = nn->b_sd[i-1][j] + (nn->biases[i-1][j] - nn->b_means[i-1][j])*(nn->biases[i-1][j] - temp);
            nn->b_means[i-1][j] = temp;
            if(nn->biases[i-1][j] < nn->b_min[i-1][j]) {
                nn->b_min[i-1][j] = nn->biases[i-1][j];
            }
            if(nn->b_max[i-1][j] < nn->biases[i-1][j]) {
                nn->b_max[i-1][j] = nn->biases[i-1][j];
            }
        }
    }
}

char *gen_name_mlp(mlp *nn, char *foldername, double learning_rate, unsigned int bits_cut, char *name) {
    unsigned int i, p;
    p = sprintf(name, "%s/nn", foldername);
    for(i = 0; i < nn->size; i++) {
        p += sprintf(&name[p], "_%d", nn->layers[i]);
    }
    sprintf(&name[p], "_%g_%d.mlp", learning_rate, bits_cut);
    return name;
}

int nan_val_mlp(mlp *nn) {
    unsigned int i;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                if(my_is_nan(nn->weights[i-1][j][k])) {
                    printf("[%u][%u][%u]\n", i-1, j, k);
                    return 1;
                }
            }
            if(my_is_nan(nn->biases[i-1][j])) {
                printf("[%u][%u]\n", i-1, j);
                return 1;
            }
        }
    }
    return 0;
}

void save_mlp(mlp *nn, char *filename) {
    FILE *f = fopen(filename, "wb");
    fprintf(f, "%u\n", nn->size);
    fwrite(nn->layers, sizeof(unsigned int), nn->size, f);
    unsigned int i;
    unsigned int t_w = 0, t_b = 0;
    for(i = 1; i < nn->size; i++) {
        t_w += nn->layers[i-1]*nn->layers[i];
        t_b += nn->layers[i];
    }
    fwrite(nn->a_w, sizeof(double), t_w, f);
    fwrite(nn->a_b, sizeof(double), t_b, f);
    fclose(f);
}

mlp *load_alloc_mlp(char *filename) {
    FILE *f = fopen(filename, "rb");
    unsigned int size;
    int r;
    r = fscanf(f, "%u\n", &size);
    unsigned int layers[size];
    r = fread(layers, sizeof(unsigned int), size, f);
    unsigned int i, t_w = 0, t_b = 0;
    for(i = 1; i < size; i++) {
        t_w += layers[i-1]*layers[i];
        t_b += layers[i];
    }
    mlp *nn = alloc_mlp(size, layers);
    r = fread(nn->a_w, sizeof(double), t_w, f);
    r = fread(nn->a_b, sizeof(double), t_b, f);
    fclose(f);
    add_stat_mlp(nn);
    return nn;
}

/**********************************************************************************************************************************/

t_fwd *alloc_t_fwd(mlp *nn) {
    t_fwd *f = (t_fwd*) malloc(sizeof(t_fwd));
    unsigned int i, t_a = 0;
    for(i = 0; i < nn->size; i++) {
        t_a += nn->layers[i]; // number of activations
    }
    f->z = (double*) malloc(sizeof(double)*(2*t_a - nn->layers[0])); // allocate memory for (z-)activations
    f->a = f->z + t_a - nn->layers[0];
    f->z_activations = (double**) malloc(sizeof(double*)*(2*nn->size - 1));
    f->activations = f->z_activations + nn->size - 1;
    f->i = 0;
    f->a_z_means = (double*) malloc(sizeof(double)*4*(t_a - nn->layers[0])); // allocate memory for statistics
    f->a_z_sd = f->a_z_means + t_a - nn->layers[0];
    f->a_z_min = f->a_z_sd + t_a - nn->layers[0];
    f->a_z_max = f->a_z_min + t_a - nn->layers[0];
    f->z_means = (double**) malloc(sizeof(double*)*4*(nn->size - 1));
    f->z_sd = f->z_means + nn->size - 1;
    f->z_min = f->z_sd + nn->size - 1;
    f->z_max = f->z_min + nn->size - 1;
    t_a = 0;
    f->activations[0] = f->a;
    for(i = 1; i < nn->size; i++) {
        f->z_activations[i-1] = f->z + t_a;
        f->activations[i] = f->a + t_a + nn->layers[0];
        f->z_means[i-1] = f->a_z_means + t_a;
        f->z_sd[i-1] = f->a_z_sd + t_a;
        f->z_min[i-1] = f->a_z_min + t_a;
        f->z_max[i-1] = f->a_z_max + t_a;
        t_a += nn->layers[i];
    }
    return f;
}

void free_t_fwd(t_fwd *f) {
    free(f->z_activations);
    free(f->z);
    free(f->a_z_means);
    free(f->z_means);
    free(f);
}

void print_t_fwd(mlp *nn, t_fwd *f) {
    unsigned int i;
    for(i = 0; i < nn->size; i++) {
        unsigned int j;
        printf("layer %d -> ", i);
        for(j = 0; j < nn->layers[i]; j++) {
            if(i > 0) {
                printf("%12.5g ", f->z_activations[i-1][j]);
            } else {
                printf("       empty ");
            }
        }
        printf("-> ");
        for(j = 0; j < nn->layers[i]; j++) {
            printf("%12.5g ", f->activations[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void reset_stat_t_fwd(mlp *nn, t_fwd *f) {
    unsigned int i;
    long inf = (0x7ffL << 52), ninf = (0xfffL << 52);
    f->i = 0;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            f->z_means[i-1][j] = 0.0;
            f->z_sd[i-1][j] = 0.0;
            f->z_min[i-1][j] = *((double*) &inf);
            f->z_max[i-1][j] = *((double*) &ninf);
        }
    }
}

void print_stat_t_fwd(mlp *nn, t_fwd *f) {
    unsigned int i;
    printf(".:T_FWD:.         mean    |      sd      |     min.     |     max.\n");
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        printf(" layer [%3d] :---------------------------------------------------------:\n", i-1);
        for(j = 0; j < nn->layers[i]; j++) {
            printf("neuron  %3d [%12.4e | %12.4e | %12.4e | %12.4e]\n", j, f->z_means[i-1][j], sqrt(f->z_sd[i-1][j]/f->i), f->z_min[i-1][j], f->z_max[i-1][j]);
        }
    }
    printf("\n");
}

void add_stat_t_fwd(mlp *nn, t_fwd *f) {
    unsigned int i;
    double temp;
    f->i++;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            temp = f->z_means[i-1][j] + (f->z_activations[i-1][j] - f->z_means[i-1][j])/f->i;
            f->z_sd[i-1][j] = f->z_sd[i-1][j] + (f->z_activations[i-1][j] - f->z_means[i-1][j])*(f->z_activations[i-1][j] - temp);
            f->z_means[i-1][j] = temp;
            if(f->z_activations[i-1][j] < f->z_min[i-1][j]) {
                f->z_min[i-1][j] = f->z_activations[i-1][j];
            }
            if(f->z_max[i-1][j] < f->z_activations[i-1][j]) {
                f->z_max[i-1][j] = f->z_activations[i-1][j];
            }
        }
    }
}

int nan_val_t_fwd(mlp *nn, t_fwd *f) {
    unsigned int i;
    for(i = 0; i < nn->layers[0]; i++) {
        if(my_is_nan(f->activations[0][i])) {
            printf("input: 0, %d\n", i);
            return 1;
        }
    }
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            if(my_is_nan(f->z_activations[i-1][j]) || my_is_nan(f->activations[i][j])) {
                printf("%d, %d\n", i-1, j);
                return 1;
            }
        }
    }
    return 0;
}

/**********************************************************************************************************************************/

t_bwd *alloc_t_bwd(mlp *nn) {
    t_bwd *b = (t_bwd*) malloc(sizeof(t_bwd));
    unsigned int i, t_w = 0, t_b = 0;
    for(i = 1; i < nn->size; i++) {
        t_w += nn->layers[i-1]*nn->layers[i]; // number of weights
        t_b += nn->layers[i]; // number of biases
    }
    b->a_w = (double*) malloc(sizeof(double)*(t_w + t_b)); // allocate memory for weights and biases
    b->a_b = b->a_w + t_w;
    b->a_wr = (double**) malloc(sizeof(double*)*(t_b + nn->size-1));
    b->biases = b->a_wr + t_b;
    b->weights = (double***) malloc(sizeof(double**)*(nn->size-1));
    b->i = 0;
    b->a_w_means = (double*) malloc(sizeof(double)*4*(t_w + t_b));  // allocate memory for statistics
    b->a_w_sd = b->a_w_means + t_w;
    b->a_w_min = b->a_w_sd + t_w;
    b->a_w_max = b->a_w_min + t_w;
    b->a_b_means = b->a_w_max + t_w;
    b->a_b_sd = b->a_b_means + t_b;
    b->a_b_min = b->a_b_sd + t_b;
    b->a_b_max = b->a_b_min + t_b;
    b->a_wr_means = (double**) malloc(sizeof(double*)*4*(t_b + nn->size - 1));
    b->a_wr_sd = b->a_wr_means + t_b;
    b->a_wr_min = b->a_wr_sd + t_b;
    b->a_wr_max = b->a_wr_min + t_b;
    b->b_means = b->a_wr_max + t_b;
    b->b_sd = b->b_means + nn->size - 1;
    b->b_min = b->b_sd + nn->size - 1;
    b->b_max = b->b_min + nn->size - 1;
    b->w_means = (double***) malloc(sizeof(double**)*4*(nn->size - 1));
    b->w_sd = b->w_means + nn->size - 1;
    b->w_min = b->w_sd + nn->size - 1;
    b->w_max = b->w_min + nn->size - 1;
    t_w = 0;
    t_b = 0;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            b->a_wr[t_b+j] = b->a_w + t_w + j*nn->layers[i-1];
            b->a_wr_means[t_b+j] = b->a_w_means + t_w + j*nn->layers[i-1];
            b->a_wr_sd[t_b+j] = b->a_w_sd + t_w + j*nn->layers[i-1];
            b->a_wr_min[t_b+j] = b->a_w_min + t_w + j*nn->layers[i-1];
            b->a_wr_max[t_b+j] = b->a_w_max + t_w + j*nn->layers[i-1];
        }
        b->weights[i-1] = b->a_wr + t_b;
        b->biases[i-1] = b->a_b + t_b;
        b->w_means[i-1] = b->a_wr_means + t_b;
        b->w_sd[i-1] = b->a_wr_sd + t_b;
        b->w_min[i-1] = b->a_wr_min + t_b;
        b->w_max[i-1] = b->a_wr_max + t_b;
        b->b_means[i-1] = b->a_b_means + t_b;
        b->b_sd[i-1] = b->a_b_sd + t_b;
        b->b_min[i-1] = b->a_b_min + t_b;
        b->b_max[i-1] = b->a_b_max + t_b;
        t_w += nn->layers[i-1]*nn->layers[i];
        t_b += nn->layers[i];
    }
    return b;
}

void free_t_bwd(t_bwd *b) {
    free(b->weights);
    free(b->a_wr);
    free(b->a_w);
    free(b->a_w_means);
    free(b->a_wr_means);
    free(b->w_means);
    free(b);
}

void print_t_bwd(mlp *nn, t_bwd *b) {
    unsigned int i;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        printf("layer %u\n", i);
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                printf("%12.5g ", b->weights[i-1][j][k]);
            }
            printf("| %12.5g\n", b->biases[i-1][j]);
        }
    }
    printf("\n");
}

void reset_stat_t_bwd(mlp *nn, t_bwd *b) {
    unsigned int i;
    long inf = (0x7ffL << 52), ninf = (0xfffL << 52);
    b->i = 0;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                b->w_means[i-1][j][k] = 0.0;
                b->w_sd[i-1][j][k] = 0.0;
                b->w_min[i-1][j][k] = *((double*) &inf);
                b->w_max[i-1][j][k] = *((double*) &ninf);
            }
            b->b_means[i-1][j] = 0.0;
            b->b_sd[i-1][j] = 0.0;
            b->b_min[i-1][j] = *((double*) &inf);
            b->b_max[i-1][j] = *((double*) &ninf);
        }
    }
}

void print_stat_t_bwd(mlp *nn, t_bwd *b) {
    unsigned int i;
    printf(".:T_BWD:.          mean    |      sd      |     min.     |     max.\n");
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        printf(" layer [%3d] :---------------------------------------------------------:\n", i-1);
        for(j = 0; j < nn->layers[i]; j++) {
            printf("neuron [%3d] :             :              :              :             :\n", j);
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                printf("  dw %3d     [%12.4e | %12.4e | %12.4e | %12.4e]\n", k, b->w_means[i-1][j][k], sqrt(b->w_sd[i-1][j][k]/b->i), b->w_min[i-1][j][k], b->w_max[i-1][j][k]);
            }
            printf(" *db         [%12.4e | %12.4e | %12.4e | %12.4e]\n", b->b_means[i-1][j], sqrt(b->b_sd[i-1][j]/b->i), b->b_min[i-1][j], b->b_max[i-1][j]);
        }
    }
    printf("\n");
}

void reset_t_bwd(mlp *nn, t_bwd *b) {
    unsigned int i;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                b->weights[i-1][j][k] = 0.0;
            }
            b->biases[i-1][j] = 0.0;
        }
    }
}

void add_stat_t_bwd(mlp *nn, t_bwd *b) {
    double temp;
    b->i++;
    int i, t_size = nn->size;
    for(i = 0; i < nn->layers[t_size - 1]; i++) {
        temp = b->b_means[t_size-2][i] + (b->biases[t_size-2][i] - b->b_means[t_size-2][i])/b->i;
        b->b_sd[t_size-2][i] = b->b_sd[t_size-2][i] + (b->biases[t_size-2][i] - b->b_means[t_size-2][i])*(b->biases[t_size-2][i] - temp);
        b->b_means[t_size-2][i] = temp;
        if(b->biases[t_size-2][i] < b->b_min[t_size-2][i]) {
            b->b_min[t_size-2][i] = b->biases[t_size-2][i];
        }
        if(b->b_max[t_size-2][i] < b->biases[t_size-2][i]) {
            b->b_max[t_size-2][i] = b->biases[t_size-2][i];
        }
        unsigned int j;
        for(j = 0; j < nn->layers[t_size-2]; j++) {
            temp = b->w_means[t_size-2][i][j] + (b->weights[t_size-2][i][j] - b->w_means[t_size-2][i][j])/b->i;
            b->w_sd[t_size-2][i][j] = b->w_sd[t_size-2][i][j] + (b->weights[t_size-2][i][j] - b->w_means[t_size-2][i][j])*(b->weights[t_size-2][i][j] - temp);
            b->w_means[t_size-2][i][j] = temp;
            if(b->weights[t_size-2][i][j] < b->w_min[t_size-2][i][j]) {
                b->w_min[t_size-2][i][j] = b->weights[t_size-2][i][j];
            }
            if(b->w_max[t_size-2][i][j] < b->weights[t_size-2][i][j]) {
                b->w_max[t_size-2][i][j] = b->weights[t_size-2][i][j];
            }
        }
    }
    for(i = t_size - 3; i >= 0; i--) {
        unsigned int j;
        for(j = 0; j < nn->layers[i+1]; j++) {
            temp = b->b_means[i][j] + (b->biases[i][j] - b->b_means[i][j])/b->i;
            b->b_sd[i][j] = b->b_sd[i][j] + (b->biases[i][j] - b->b_means[i][j])*(b->biases[i][j] - temp);
            b->b_means[i][j] = temp;
            if(b->biases[i][j] < b->b_min[i][j]) {
                b->b_min[i][j] = b->biases[i][j];
            }
            if(b->b_max[i][j] < b->biases[i][j]) {
                b->b_max[i][j] = b->biases[i][j];
            }
            unsigned int k;
            for(k = 0; k < nn->layers[i]; k++) {
                temp = b->w_means[i][j][k] + (b->weights[i][j][k] - b->w_means[i][j][k])/b->i;
                b->w_sd[i][j][k] = b->w_sd[i][j][k] + (b->weights[i][j][k] - b->w_means[i][j][k])*(b->weights[i][j][k] - temp);
                b->w_means[i][j][k] = temp;
                if(b->weights[i][j][k] < b->w_min[i][j][k]) {
                    b->w_min[i][j][k] = b->weights[i][j][k];
                }
                if(b->w_max[i][j][k] < b->weights[i][j][k]) {
                    b->w_max[i][j][k] = b->weights[i][j][k];
                }
            }
        }
    }
}

int nan_val_t_bwd(mlp *nn, t_bwd *b) {
    unsigned int i;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                if(my_is_nan(b->weights[i-1][j][k])) {
                    return 1;
                }
            }
            if(my_is_nan(b->biases[i-1][j])) {
                return 1;
            }
        }
    }
    return 0;
}

void add_t_bwd(mlp *nn, t_bwd *b, t_bwd *b2) {
    unsigned int i;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                b->weights[i-1][j][k] = CUT_BITS_double(b->weights[i-1][j][k] + b2->weights[i-1][j][k]);
            }
            b->biases[i-1][j] = CUT_BITS_double(b->biases[i-1][j] + b2->biases[i-1][j]);
        }
    }
}

void mul_add_t_bwd(mlp *nn, t_bwd *b, t_bwd *b2, double f) {
    unsigned int i;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                b->weights[i-1][j][k] = CUT_BITS_double(b->weights[i-1][j][k] + f*b2->weights[i-1][j][k]);
            }
            b->biases[i-1][j] = CUT_BITS_double(b->biases[i-1][j] + f*b2->biases[i-1][j]);
        }
    }
}

double normL2_t_bwd(mlp *nn, t_bwd *b) {
    double norm = 0.0;
    unsigned int i;
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            unsigned int k;
            double temp = b->biases[i-1][j];
            norm += temp*temp;
            for(k = 0; k < nn->layers[i-1]; k++) {
                temp = b->weights[i-1][j][k];
                norm += temp*temp;
            }
        }
    }
    return sqrt(norm);
}

/**********************************************************************************************************************************/

double *forward(mlp *nn, t_fwd *f, fun_tab *sigma_tab, double *in) {
    double temp;
    unsigned int i, m = 0;
    for(i = 0; i < nn->layers[0]; i++) {
        f->activations[0][i] = CUT_BITS_double(in[i]);
    }
    for(i = 1; i < nn->size; i++) {
        unsigned int j;
        for(j = 0; j < nn->layers[i]; j++) {
            f->z_activations[i-1][j] = CUT_BITS_double(nn->biases[i-1][j]);
            unsigned int k;
            for(k = 0; k < nn->layers[i-1]; k++) {
                f->z_activations[i-1][j] = CUT_BITS_double(f->z_activations[i-1][j] + nn->weights[i-1][j][k]*f->activations[i-1][k]);
            }
            //f->activations[i][j] = CUT_BITS_double(sigma(f->z_activations[i-1][j]));
            f->activations[i][j] = CUT_BITS_double(f_fun_tab(f->z_activations[i-1][j], sigma_tab));
        }
    }
    add_stat_t_fwd(nn, f);
    return f->activations[nn->size-1];
}

void backprop(mlp *nn, t_fwd *f, t_bwd *b, fun_tab *sigma_tab, fun_tab *d_sigma_tab, double *in, double *out) {
    forward(nn, f, sigma_tab, in);
    double temp;
    int i;
    unsigned int t_size = nn->size;
    for(i = 0; i < nn->layers[t_size - 1]; i++) {
        //b->biases[t_size-2][i] = CUT_BITS_double((f->activations[t_size - 1][i] - out[i]) * d_sigma(f->z_activations[t_size - 2][i]));
        b->biases[t_size-2][i] = CUT_BITS_double((f->activations[t_size - 1][i] - out[i]) * f_fun_tab(f->z_activations[t_size - 2][i], d_sigma_tab));
        unsigned int j;
        for(j = 0; j < nn->layers[t_size-2]; j++) {
            b->weights[t_size-2][i][j] = CUT_BITS_double(b->biases[t_size - 2][i] * f->activations[t_size - 2][j]);
        }
    }
    for(i = t_size - 3; i >= 0; i--) {
        unsigned int j;
        for(j = 0; j < nn->layers[i+1]; j++) {
            temp = 0.0;
            unsigned int k;
            for(k = 0; k < nn->layers[i+2]; k++) {
                temp = CUT_BITS_double(temp + b->biases[i+1][k] * nn->weights[i+1][k][j]);
            }
            //b->biases[i][j] = CUT_BITS_double(d_sigma(f->z_activations[i][j]) * temp);
            b->biases[i][j] = CUT_BITS_double(f_fun_tab(f->z_activations[i][j], d_sigma_tab) * temp);
            for(k = 0; k < nn->layers[i]; k++) {
                b->weights[i][j][k] = CUT_BITS_double(b->biases[i][j] * f->activations[i][k]);
            }
        }
    }
    add_stat_t_bwd(nn, b);
}

/**********************************************************************************************************************************/

void cost_x(mlp *nn, t_fwd *f, fun_tab *sigma_tab, double *in, double *out, double *cost, int *tp) {
    double cost_t = 0.0, *a = forward(nn, f, sigma_tab, in);
    double out_max = out[0], a_max = a[0];
    int i, L = nn->layers[nn->size - 1], out_argmax = 0, a_argmax = 0;
    for(i = 0; i < L; i++) {
        double temp = out[i] - a[i];
        cost_t += temp*temp;
        if(out_max < out[i]) {
            out_max = out[i];
            out_argmax = i;
        }
        if(a_max < a[i]) {
            a_max = a[i];
            a_argmax = i;
        }
    }
    *cost = cost_t/2.0;
    *tp = (L == 1) ? (out[0] < 0.5 && a[0] < 0.5) || (out[0] >= 0.5 && a[0] >= 0.5) : out_argmax == a_argmax;
}

void evaluate(mlp *nn, t_fwd *f, fun_tab *sigma_tab, dataset *set, double *cost, double *tpr) {
    double cost_sum = 0.0;
    int i, tp_sum = 0;
    for(i = 0; i < set->size; i++) {
        double cost_t;
        int tp_t;
        cost_x(nn, f, sigma_tab, GET_INPUT(set, i), GET_OUTPUT(set, i), &cost_t, &tp_t);
        cost_sum += cost_t;
        tp_sum += tp_t;
    }
    *cost = cost_sum/set->size;
    *tpr = ((double) tp_sum)/set->size;
}

/**********************************************************************************************************************************/

void init_for_train(mlp *nn, mlp **nn2, mlp **nn_opt, t_fwd **f, t_bwd **grad, t_bwd **b, fun_tab **sigma_tab, fun_tab **d_sigma_tab) {
    if(nn2) {
        *nn2 = alloc_mlp(nn->size, nn->layers);
    }
    *nn_opt = alloc_mlp(nn->size, nn->layers);
    *f = alloc_t_fwd(nn);
    *grad = alloc_t_bwd(nn);
    *b = alloc_t_bwd(nn);
    *sigma_tab = alloc_fun_tab(fun_tab_low, fun_tab_high, fun_tab_size, sigma);
    *d_sigma_tab = alloc_fun_tab(fun_tab_low, fun_tab_high, fun_tab_size, d_sigma);

    reset_stat_mlp(nn); // mlp weights and biases mean and sd statistics
    reset_stat_t_fwd(nn, *f); // t_fwd z_activation mean and sd statistics
    reset_stat_t_bwd(nn, *b); // t_bwd weights and biases mean and sd statistics
}

void free_from_train(mlp *nn, mlp *nn2, mlp *nn_opt, t_fwd *f, t_bwd *grad, t_bwd *b, fun_tab *sigma_tab, fun_tab *d_sigma_tab) {
    //print_stat_mlp(nn); // mlp weights and biases: mean and sd statistics
    //print_stat_t_fwd(nn, f); // t_fwd z_activation: mean and sd statistics
    //print_stat_t_bwd(nn, b); // t_bwd weights and biases: mean and sd statistics
    if(nn2) {
        free_mlp(nn2);
    }
    init_mlp_from_mlp(nn, nn_opt);
    free_mlp(nn_opt);
    free_t_fwd(f);
    free_t_bwd(grad);
    free_t_bwd(b);
    free_fun_tab(sigma_tab);
    free_fun_tab(d_sigma_tab);
}

void batch_derivative(mlp *nn, t_fwd *f, t_bwd *grad, t_bwd *b, fun_tab *sigma_tab, fun_tab *d_sigma_tab, unsigned int batch_size, unsigned int batch_start, dataset *train_set) {
    reset_t_bwd(nn, grad);
    unsigned int i, batch_end = batch_start + batch_size;
    if(batch_end > train_set->size) {
        batch_end = train_set->size;
    }
    for(i = batch_start; i < batch_end; i++) {
        backprop(nn, f, b, sigma_tab, d_sigma_tab, GET_INPUT(train_set, i), GET_OUTPUT(train_set, i));
        add_t_bwd(nn, grad, b);
    }
}

/**********************************************************************************************************************************/

void batch_grad_desc(mlp *nn, t_fwd *f, t_bwd *grad, t_bwd *b, fun_tab *sigma_tab, fun_tab *d_sigma_tab, unsigned int batch_size, unsigned int batch_start, double learning_rate, dataset *train_set) {
    batch_derivative(nn, f, grad, b, sigma_tab, d_sigma_tab, batch_size, batch_start, train_set);
    grad_desc(nn, nn, grad, learning_rate);
    add_stat_mlp(nn);
}

void batch_gss(mlp *nn, mlp *nn2, t_fwd *f, t_bwd *grad, t_bwd *b, fun_tab *sigma_tab, fun_tab *d_sigma_tab, unsigned int batch_size, unsigned int batch_start, dataset *train_set) {
    batch_derivative(nn, f, grad, b, sigma_tab, d_sigma_tab, batch_size, batch_start, train_set);
    double learning_rate = gss(nn, nn2, f, grad, sigma_tab, train_set);
    //printf("batch start: %u | learning rate: %g\n", batch_start, learning_rate);
    grad_desc(nn, nn, grad, learning_rate);
    add_stat_mlp(nn);
}

void batch_adam(mlp *nn, t_fwd *f, t_bwd *grad, t_bwd *b, t_bwd *m, t_bwd *v, fun_tab *sigma_tab, fun_tab *d_sigma_tab, unsigned int batch_size, unsigned int batch_start, double learning_rate, double b1, double b2, double pow_b1, double pow_b2, dataset *train_set) {
    batch_derivative(nn, f, grad, b, sigma_tab, d_sigma_tab, batch_size, batch_start, train_set);
    adam(nn, grad, m, v, learning_rate, b1, b2, pow_b1, pow_b2);
    add_stat_mlp(nn);
}

void train_epochs_grad_desc(mlp *nn, unsigned int epochs, unsigned int batch_size, double learning_rate, dataset *set) {
    t_fwd *f;
    t_bwd *b, *grad;
    fun_tab *sigma_tab, *d_sigma_tab;
    dataset *train_set, *valid_set;
    mlp *nn_opt;
    balanced_split_class_dataset(set, &train_set, &valid_set, 0.8);
    init_for_train(nn, 0, &nn_opt, &f, &grad, &b, &sigma_tab, &d_sigma_tab);
    double cost, tpr;
    clock_t time = clock();
    evaluate(nn, f, sigma_tab, valid_set, &cost, &tpr);
    printf("epochs: %d\nbatch size: %d\nlearning rate: %g\nbits cut: %u\nlayers: ", epochs, batch_size, learning_rate, bits_cut);
    int i;
    for(i = 0; i < nn->size; i++) {
        printf((i == nn->size - 1) ? "%d\n" : "%d, ", nn->layers[i]);
    }
    printf("|  epoch |  cost(MSE) | TPR(%%) | time(sec) |\n");
    printf("|   init | %10.6f | %6.4f | %9.2f |\n", cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    double cost_opt = cost;
    unsigned int epoch;
    for(epoch = 0; epoch < epochs; epoch++) {
        time = clock();
        shuffle(train_set);
        for(i = 0; i < train_set->size; i += batch_size) {
            batch_grad_desc(nn, f, grad, b, sigma_tab, d_sigma_tab, batch_size, i, learning_rate, train_set);
        }
        evaluate(nn, f, sigma_tab, valid_set, &cost, &tpr);
        if(cost < cost_opt) {
            cost_opt = cost;
            init_mlp_from_mlp(nn_opt, nn);
        }
        printf("|%6d  | %10.6f | %6.4f | %9.2g |\n", epoch, cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    }
    free_from_train(nn, 0, nn_opt, f, grad, b, sigma_tab, d_sigma_tab);
    free_dataset(train_set); free_dataset(valid_set);
}

void train_epochs_gss(mlp *nn, unsigned int epochs, unsigned int batch_size, dataset *set) {
    extern double gss_step, gss_mul, gss_tol;
    gss_step = 0.01;
    gss_mul = 1.4;
    gss_tol = 1e-2;
    t_fwd *f;
    t_bwd *b, *grad;
    fun_tab *sigma_tab, *d_sigma_tab;
    dataset *train_set, *valid_set;
    mlp *nn2, *nn_opt;
    balanced_split_class_dataset(set, &train_set, &valid_set, 0.8);
    init_for_train(nn, &nn2, &nn_opt, &f, &grad, &b, &sigma_tab, &d_sigma_tab);
    //train_set = set; valid_set = set;
    double cost, tpr;
    clock_t time = clock();
    evaluate(nn, f, sigma_tab, valid_set, &cost, &tpr);
    printf("epochs: %d\nbatch size: %d\nlearning rate: golden section search\nbits cut: %u\nlayers: ", epochs, batch_size, bits_cut);
    int i;
    for(i = 0; i < nn->size; i++) {
        printf((i == nn->size - 1) ? "%d\n" : "%d, ", nn->layers[i]);
    }
    printf("|  epoch |  cost(MSE) | TPR(%%) | time(sec) |\n");
    printf("|   init | %10.6f | %6.4f | %9.2f |\n", cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    double cost_opt = cost;
    unsigned int epoch;
    for(epoch = 0; epoch < epochs; epoch++) {
        time = clock();
        shuffle(train_set);
        for(i = 0; i < train_set->size; i += batch_size) {
            batch_gss(nn, nn2, f, grad, b, sigma_tab, d_sigma_tab, batch_size, i, train_set);
        }
        evaluate(nn, f, sigma_tab, valid_set, &cost, &tpr);
        if(cost < cost_opt) {
            cost_opt = cost;
            init_mlp_from_mlp(nn_opt, nn);
        }
        printf("|%6d  | %10.6f | %6.4f | %9.2g |\n", epoch, cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    }
    free_from_train(nn, nn2, nn_opt, f, grad, b, sigma_tab, d_sigma_tab);
    free_dataset(train_set); free_dataset(valid_set);
}

void train_epochs_adam(mlp *nn, unsigned int epochs, unsigned int batch_size, double learning_rate, double b1, double b2, dataset *set) {
    extern double adam_epsilon;
    adam_epsilon = 1e-8;
    t_fwd *f;
    t_bwd *b, *grad, *m = alloc_t_bwd(nn), *v = alloc_t_bwd(nn);
    fun_tab *sigma_tab, *d_sigma_tab;
    dataset *train_set, *valid_set;
    mlp *nn_opt;
    init_for_train(nn, 0, &nn_opt, &f, &grad, &b, &sigma_tab, &d_sigma_tab);
    reset_t_bwd(nn, m); reset_t_bwd(nn, v);
    balanced_split_class_dataset(set, &train_set, &valid_set, 0.8);
    //train_set = set; valid_set = set;
    double cost, tpr;
    clock_t time = clock();
    evaluate(nn, f, sigma_tab, valid_set, &cost, &tpr);
    printf("epochs: %d\nbatch size: %d\nlearning rate: %g\nbits cut: %u\nlayers: ", epochs, batch_size, learning_rate, bits_cut);
    int i;
    for(i = 0; i < nn->size; i++) {
        printf((i == nn->size - 1) ? "%d\n" : "%d, ", nn->layers[i]);
    }
    printf("|  epoch |  cost(MSE) | TPR(%%) | time(sec) |\n");
    printf("|   init | %10.6f | %6.4f | %9.2f |\n", cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    double cost_opt = cost, pow_b1 = b1, pow_b2 = b2;
    unsigned int epoch;
    for(epoch = 0; epoch < epochs; epoch++) {
        time = clock();
        shuffle(train_set);
        for(i = 0; i < train_set->size; i += batch_size) {
            batch_adam(nn, f, grad, b, m, v, sigma_tab, d_sigma_tab, batch_size, i, learning_rate, b1, b2, pow_b1, pow_b2, train_set);
        }
        pow_b1 *= b1;
        pow_b2 *= b2;
        evaluate(nn, f, sigma_tab, valid_set, &cost, &tpr);
        if(cost < cost_opt) {
            cost_opt = cost;
            init_mlp_from_mlp(nn_opt, nn);
        }
        printf("|%6d  | %10.6f | %6.4f | %9.2g |\n", epoch, cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    }
    free_from_train(nn, 0, nn_opt, f, grad, b, sigma_tab, d_sigma_tab);
    free_t_bwd(m); free_t_bwd(v);
    free_dataset(train_set); free_dataset(valid_set);
}

void train_grad_desc(mlp *nn, unsigned int batch_size, double learning_rate, dataset *set) {
    t_fwd *f;
    t_bwd *b, *grad;
    fun_tab *sigma_tab, *d_sigma_tab;
    dataset *train_set, *valid_set;
    mlp *nn_opt;
    init_for_train(nn, 0, &nn_opt, &f, &grad, &b, &sigma_tab, &d_sigma_tab);
    balanced_split_class_dataset(set, &train_set, &valid_set, 0.8);
    printf("TABULATED FUNCTIONS:\n");
    printf("range: [%g, %g]\n", sigma_tab->start, sigma_tab->end);
    printf("step: %g\n\n", sigma_tab->step);
    double cost, tpr;
    clock_t time = clock();
    evaluate(nn, f, sigma_tab, valid_set, &cost, &tpr);
    printf("batch size: %d\nlearning rate: %g\nbits cut: %u\nlayers: ", batch_size, learning_rate, bits_cut);
    int i;
    for(i = 0; i < nn->size; i++) {
        printf((i == nn->size - 1) ? "%d\n" : "%d, ", nn->layers[i]);
    }
    printf("\n");
    printf("|  epoch |  cost(MSE) | TPR(%%) | time(sec) |\n");
    printf("|   init | %10.6f | %6.4f | %9.2f |\n", cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    double cost_opt = cost;
    unsigned int epoch = 0, epoch_opt = 0, epoch_stop = 1;
    while(1) {
        time = clock();
        shuffle(train_set);
        for(i = 0; i < train_set->size; i += batch_size) {
            batch_grad_desc(nn, f, grad, b, sigma_tab, d_sigma_tab, batch_size, i, learning_rate, train_set);
        }
        evaluate(nn, f, sigma_tab, valid_set, &cost, &tpr);
        printf("|%6d  | %10.6f | %6.4f | %9.2f |\n", epoch, cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
        if(cost < cost_opt) {
            cost_opt = cost;
            epoch_opt = epoch;
            epoch_stop = (unsigned int) (1.1*epoch + 1);
            init_mlp_from_mlp(nn_opt, nn);
        }
        if(epoch == epoch_stop) {
            break;
        }
        epoch++;
    }
    free_from_train(nn, 0, nn_opt, f, grad, b, sigma_tab, d_sigma_tab);
    free_dataset(train_set); free_dataset(valid_set);
}

void train_adam(mlp *nn, unsigned int epochs, unsigned int batch_size, double learning_rate, double b1, double b2, dataset *set) {
    extern double adam_epsilon;
    adam_epsilon = 1e-8;
    t_fwd *f;
    t_bwd *b, *grad, *m = alloc_t_bwd(nn), *v = alloc_t_bwd(nn);
    fun_tab *sigma_tab, *d_sigma_tab;
    dataset *train_set, *valid_set;
    mlp *nn_opt;
    init_for_train(nn, 0, &nn_opt, &f, &grad, &b, &sigma_tab, &d_sigma_tab);
    balanced_split_class_dataset(set, &train_set, &valid_set, 0.8);
    //train_set = set;
    //valid_set = set;
    printf("TABULATED FUNCTIONS:\n");
    printf("range: [%g, %g]\n", sigma_tab->start, sigma_tab->end);
    printf("step: %g\n\n", sigma_tab->step);
    double cost, tpr;
    clock_t time = clock();
    evaluate(nn, f, sigma_tab, valid_set, &cost, &tpr);
    printf("batch size: %d\nlearning rate: %g\nbits cut: %u\nlayers: ", batch_size, learning_rate, bits_cut);
    int i;
    for(i = 0; i < nn->size; i++) {
        printf((i == nn->size - 1) ? "%d\n" : "%d, ", nn->layers[i]);
    }
    printf("\n");
    printf("|  epoch |  cost(MSE) | TPR(%%) | time(sec) |\n");
    printf("|   init | %10.6f | %6.4f | %9.2f |\n", cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    double cost_opt = cost, pow_b1 = b1, pow_b2 = b2;
    unsigned int epoch = 0, epoch_opt = 0, epoch_stop = 1;
    while(1) {
        time = clock();
        shuffle(train_set);
        for(i = 0; i < train_set->size; i += batch_size) {
            batch_adam(nn, f, grad, b, m, v, sigma_tab, d_sigma_tab, batch_size, i, learning_rate, b1, b2, pow_b1, pow_b2, train_set);
        }
        pow_b1 *= b1;
        pow_b2 *= b2;
        evaluate(nn, f, sigma_tab, valid_set, &cost, &tpr);
        printf("|%6d  | %10.6f | %6.4f | %9.2f |\n", epoch, cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
        if(cost < cost_opt) {
            cost_opt = cost;
            epoch_opt = epoch;
            epoch_stop = (unsigned int) (1.1*epoch + 1);
            init_mlp_from_mlp(nn_opt, nn);
        }
        if(epoch == epoch_stop) {
            break;
        }
        epoch++;
    }
    free_from_train(nn, 0, nn_opt, f, grad, b, sigma_tab, d_sigma_tab);
    free_t_bwd(m); free_t_bwd(v);
    free_dataset(train_set); free_dataset(valid_set);
}
