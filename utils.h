#ifndef UTILS_INCLUDE
#define UTILS_INCLUDE

    #include <stdio.h>
    #include <stdlib.h>
    #include <math.h>

    #include "mul_perceptron.h"
    #include "fixed_point.h"


    typedef struct mlp mlp;

    typedef struct fun_tab {
        unsigned int size;
        double start, end, step, *y;
    } fun_tab;

    typedef struct q_fun_tab {
        unsigned int size;
        long start, end, step, *x, *y;
    } q_fun_tab;

    double fun_tab_low, fun_tab_high;
    unsigned int fun_tab_size;

    void set_fun_tab(double, double, unsigned int);

    double lin_int(double, double, double, double, double);
    long q_lin_int(long, long, long, long, long);

    unsigned int find_bis(double, double, double, double, unsigned int);
    unsigned int q_find_bis(long, long*, unsigned int);

    fun_tab *alloc_fun_tab(double, double, unsigned int, double (*)(double));
    fun_tab *alloc_load_fun_tab(char*);
    double f_fun_tab(double, fun_tab*);
    void save_fun_tab(char*, fun_tab*);
    void gen_save_fun_tab(double, double, unsigned int, char*, double (*)(double));
    void free_fun_tab(fun_tab*);

    q_fun_tab *alloc_q_fun_tab(double, double, unsigned int, double (*)(double));
    q_fun_tab *alloc_load_q_fun_tab(char*);
    long f_q_fun_tab(long, q_fun_tab*);
    void save_q_fun_tab(char*, q_fun_tab*);
    void gen_save_q_fun_tab(long, long, unsigned int, char*, double (*)(double));
    void free_q_fun_tab(q_fun_tab*);

    unsigned int my_argmax(int, double*);
    unsigned int my_argmin(int, double*);
    double my_abs(double);
    long my_round(double);
    int my_is_nan(double);

    int my_q_argmax(int, long*);

    double array_mean(int, double*);
    double array_sd(int, double, double*);

    // display unsigned long as binary number
    void print_bits(unsigned long);

    // Mitchell's algortihm
    unsigned long ulong_ma_mul(unsigned long, unsigned long);
    long long_ma_mul(long, long);

    // Iterative Logarithmic Multiplier with 1 correction iteration
    unsigned long ulong_ilm(unsigned long, unsigned long);
    long long_ilm(long, long);

#endif