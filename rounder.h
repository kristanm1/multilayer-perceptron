#ifndef ROUNDER_INCLUDE
#define ROUNDER_INCLUDE

    extern int bits_cut;

    // set last _n_ bits to zero
    #define CUT_BITS_double(VAL_M) ({ \
        double VAL = VAL_M; \
        unsigned long VALi = (*((unsigned long*) &VAL) & (-1L << bits_cut)); \
        *((double*) & VALi); \
    })

#endif