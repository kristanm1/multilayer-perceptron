#ifndef LOAD_MNIST_INCLUDE
#define LOAD_MNIST_INCLUDE

    #include "dataset.h"

    #include <stdio.h>
    #include <stdlib.h>

/***************************************************
    MNIST link: http://yann.lecun.com/exdb/mnist/
****************************************************
****************************************************/

    // convert four consecutive uchars from array to uint
    #define UCHAR_TO_UINT(a) ((a[0] << 24) + (a[1] << 16) + (a[2] << 8) + a[3])
    // get p-th image from MNIST images
    #define GET_IMAGE(imgs, p) (&imgs->data[p * (imgs->height * imgs->width)])


    // magic_number: 0x00000801(2049)
    // size: number of labels
    // data
    typedef struct labels {
        unsigned int magic_number, size;
        unsigned char *data;
    } labels;

    // magic_number: 0x00000803(2051)
    // size: number of images
    // height, width
    // data
    typedef struct images {
        unsigned int magic_number, size, height, width;
        unsigned char *data;
    } images;


    // load labels from disk
    labels *load_labels(char*);
    void free_labels(labels*);
    
    // load images from disk
    images *load_images(char*);
    void free_images(images*);

    // save p-th image from images to disk
    void save_image(images*, unsigned int, char*);

    // usage -> test_MNIST(argc, argv);
    // example: argc = 4, argv = {"17", "54", "4", "6730"}
    void test_MNIST(int, char*[]);

#endif