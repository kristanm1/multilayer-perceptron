#include "test.h"

void eval_MNIST(mlp *nn, dataset *set) {
    double cost, tpr;
    t_fwd *f = alloc_t_fwd(nn);
    fun_tab *tab = alloc_fun_tab(fun_tab_low, fun_tab_high, fun_tab_size, sigma);
    clock_t time = clock();
    evaluate(nn, f, tab, set, &cost, &tpr);
    printf("| hidden | %10.6f | %6.4f | %9.2f |\n", cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    free_t_fwd(f);
    free_fun_tab(tab);
}

void train_MNIST(int argc, char *argv[]) {
    if(argc > 4) {

        extern int bits_cut;

        unsigned int epochs = atoi(argv[1]);
        unsigned int batch_size = atoi(argv[2]);
        double learning_rate = atof(argv[3]);
        bits_cut = atoi(argv[4]);
        //int seed = time(0);
        //srand(123);

        char *labs_train = "./MNIST_unzipped/train-labels.idx1-ubyte";
        char *imgs_train = "./MNIST_unzipped/train-images.idx3-ubyte";
        char *labs_hidden = "./MNIST_unzipped/t10k-labels.idx1-ubyte";
        char *imgs_hidden = "./MNIST_unzipped/t10k-images.idx3-ubyte";

        dataset *train_set = alloc_MNIST_dataset(labs_train, imgs_train);
        dataset *hidden_set = alloc_MNIST_dataset(labs_hidden, imgs_hidden);

        dataset *train_subset = balanced_subset_class_dataset(train_set, 10000);
        free_dataset(train_set);
        train_set = train_subset;

        //unsigned int nn_size = 3, nn_layers[] = {784, 30, 10};
        //mlp *nn = alloc_mlp(nn_size, nn_layers);
        //init_rand_mlp(nn, 0.1);
        // or load_alloc_mlp..
        mlp *nn = load_alloc_mlp("weights/init.mlp");

        //train_epochs_grad_desc(nn, epochs, batch_size, learning_rate, train_set);               // ./main 5 10 0.1 0
        //train_epochs_gss(nn, epochs, batch_size, train_set);                                    // ./main 5 1000 0.0 0
        train_epochs_adam(nn, epochs, batch_size, learning_rate, 0.9, 0.95, train_set);         // ./main 5 10 0.002 0

        //train_grad_desc(nn, batch_size, learning_rate, train_set);                              // ./main 5 10 0.1 0
        //train_adam(nn, epochs, batch_size, learning_rate, 0.9, 0.95, train_set);              // ./main 5 10 0.002 0

        eval_MNIST(nn, hidden_set);

        // save_mlp...
        //char filename[MAX_FILENAME_LENGTH];
        //save_mlp(nn, gen_name_mlp(nn, "weights", learning_rate, bits_cut, filename));

        free_mlp(nn);
        free_dataset(train_set);
        free_dataset(hidden_set);
    }
}

void q_eval_MNIST(q_mlp *q_nn, q_dataset *q_set) {
    double cost, tpr;
    q_t_fwd *q_f = alloc_q_t_fwd(q_nn);
    q_fun_tab *q_tab = alloc_q_fun_tab(fun_tab_low, fun_tab_high, fun_tab_size, sigma);
    clock_t time = clock();
    q_evaluate(q_nn, q_f, q_tab, q_set, &cost, &tpr);
    printf("| hidden | %10.6f | %6.4f | %9.2f |\n", cost, tpr, ((double) (clock() - time))/CLOCKS_PER_SEC);
    free_q_t_fwd(q_f);
    free_q_fun_tab(q_tab);
}

void q_train_MNIST(int argc, char *argv[]) {
    if(argc > 5) {

        extern int integer_bits, fraction_bits;

        unsigned int epochs = atoi(argv[1]);
        unsigned int batch_size = atoi(argv[2]);
        q_set_num_bits(atoi(argv[4]), atoi(argv[5]));
        long q_learning_rate = double_to_fixed(atof(argv[3]));
        long q_b1 = double_to_fixed(0.9), q_b2 = double_to_fixed(0.95);

        char *labs_train = "./MNIST_unzipped/train-labels.idx1-ubyte";
        char *imgs_train = "./MNIST_unzipped/train-images.idx3-ubyte";
        char *labs_hidden = "./MNIST_unzipped/t10k-labels.idx1-ubyte";
        char *imgs_hidden = "./MNIST_unzipped/t10k-images.idx3-ubyte";

        dataset *train_set = alloc_MNIST_dataset(labs_train, imgs_train);
        dataset *hidden_set = alloc_MNIST_dataset(labs_hidden, imgs_hidden);
        dataset *train_subset = balanced_subset_class_dataset(train_set, 10000);
        free_dataset(train_set);
        q_dataset *q_hidden_set = alloc_q_dataset_from_dataset(hidden_set);
        train_set = train_subset;
        free_dataset(hidden_set);

        //unsigned int nn_size = 3, nn_layers[] = {784, 30, 10};
        mlp *nn = load_alloc_mlp("weights/init.mlp");
        q_mlp *q_nn = alloc_q_mlp_from_mlp(nn);
        free_mlp(nn);

        //q_train_epochs_grad_desc(q_nn, epochs, batch_size, q_learning_rate, train_set);           // ./main 5 10 0.1 6 26
        //q_train_epochs_adam(q_nn, epochs, batch_size, q_learning_rate, q_b1, q_b2, train_set);    // ./main 5 10 0.002 6 26
        
        //q_train_grad_desc(q_nn, batch_size, q_learning_rate, train_set);
        q_train_adam(q_nn, batch_size, q_learning_rate, double_to_fixed(0.9), double_to_fixed(0.95), train_set);

        q_eval_MNIST(q_nn, q_hidden_set);

        free_q_mlp(q_nn);
        free_dataset(train_set);
        free_q_dataset(q_hidden_set);
    }
}

int main(int argc, char *argv[]) {

    set_fun_tab(-8.0, 8.0, 50000);

    //test_mattmazur();

    // ./main 300 1 2.7 0 -> tpr = 1.0
    //test_xor(argc, argv);

    // ./main 300 1 2.7 4 14 -> tpr = 1.0
    //test_q_xor(argc, argv);

    //print_bits(0xF1234L);
    //test_rounder();

    //test_dataset();
    //test_datasubset();
    //test_q_dataset();
    //test_MNIST_dataset_and_q_dataset();

    //test_avg_std();
    //test_MNIST_avg_std();

    //test_fixed_point();

    //test_fun_tab();

    //test_mattmazur_mlp_and_q_mlp();
    //test_random_mlp_and_q_mlp();
    //test_batch_derivative();

    //test_moving_average();

    //test_multipliers();

    //test_gss();

    // save tabulated function y=exp(x) on disk
    //gen_save_exp_tab(-11, 11, 10000, "exp_tab.tab");
    //gen_save_q_exp_tab(-11, 11, 10000, "q_exp_tab.tab");

    // ./main <epochs> <batch_size> <learning_rate> <bits_cut>
    // ./main 20 10 0.1 40
    // tested info.: alloc_fun_tab(-4, 4, 10000, (d_)sigma)
    //               pri 10000 ucnih primerih: cost = 0.068421, tpr = 0.8890
    train_MNIST(argc, argv);

    // integer bits = 3, fraction bits = 13
    // ./main 20 10 0.01 3 13
    // tested info.: alloc_q_fun_tab(-4, 4, 10000, (d_)sigma)
    //               pri 10000 ucnih primerih: cost = 0.068645, tpr = 0.9035
    //q_train_MNIST(argc, argv);

    return 0;
}