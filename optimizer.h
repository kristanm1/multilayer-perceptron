#ifndef OPTIMIZER_INCLUDE
#define OPTIMIZER_INCLUDE

    #include "utils.h"
    #include "mul_perceptron.h"


    // gss (golden section seatch)
    // constants
    #define INV_GOLD_RATIO 0.61803398874989484820
    #define INV_SQ_GOLD_RATIO 0.38196601125010515180
    // parameters
    double gss_step, gss_mul, gss_tol;

    // adadelta
    // constants
    double adam_epsilon;

    typedef struct fun_tab fun_tab;
    typedef struct mlp mlp;
    typedef struct t_fwd t_fwd;
    typedef struct t_bwd t_bwd;

// MULTIVARIABLE OPTIMIZATION

    // ADAM reference: https://arxiv.org/abs/1412.6980

    void grad_desc(mlp*, mlp*, t_bwd*, double);
    void adam(mlp*, t_bwd*, t_bwd*, t_bwd*, double, double, double, double, double);

// SINGLE VARIABLE OPTIMIZATION

    // golden section search
    double gss(mlp*, mlp*, t_fwd*, t_bwd*, fun_tab*, dataset*);

#endif