import numpy as np


def parse_mlp(filename):
    with open(filename, 'r') as file:
        # parse arguments
        epochs = int(file.readline().split(':')[1])
        batch_size = int(file.readline().split(':')[1])
        learning_rate = float(file.readline().split(':')[1])
        bits_cut = int(file.readline().split(':')[1])
        layers = [int(i) for i in file.readline().split(':')[1].split(',')]
        # parse learning
        cost_tpr_time = np.zeros((epochs + 1, 3))
        file.readline()
        for i in range(epochs + 1):
            sline = file.readline().rstrip().split('|')
            cost_tpr_time[i, 0] = float(sline[2])
            cost_tpr_time[i, 1] = float(sline[3])
            cost_tpr_time[i, 2] = float(sline[4])
        # parse mlp statistics
        mlp_stat = [np.zeros((layers[i-1] + 1, layers[i], 4)) for i in range(1, len(layers))]
        file.readline()
        for i in range(1, len(layers)):
            file.readline()
            for j in range(layers[i]):
                file.readline()
                for k in range(layers[i-1] + 1):
                    sline = file.readline().rstrip().split('|')
                    mlp_stat[i-1][k, j, 0] = float(sline[0].split('[')[1])
                    mlp_stat[i-1][k, j, 1] = float(sline[1])
                    mlp_stat[i-1][k, j, 2] = float(sline[2])
                    mlp_stat[i-1][k, j, 3] = float(sline[3].split(']')[0])
        # parse fwd statistics
        fwd_stat = [np.zeros((layers[i], 4)) for i in range(1, len(layers))]
        file.readline()
        file.readline()
        for i in range(1, len(layers)):
            file.readline()
            for j in range(layers[i]):
                sline = file.readline().rstrip().split('|')
                fwd_stat[i-1][j, 0] = float(sline[0].split('[')[1])
                fwd_stat[i-1][j, 1] = float(sline[1])
                fwd_stat[i-1][j, 2] = float(sline[2])
                fwd_stat[i-1][j, 3] = float(sline[3].split(']')[0])
        # parse bwd statistics
        bwd_stat = [np.zeros((layers[i-1] + 1, layers[i], 4)) for i in range(1, len(layers))]
        file.readline()
        file.readline()
        for i in range(1, len(layers)):
            file.readline()
            for j in range(layers[i]):
                file.readline()
                for k in range(layers[i-1] + 1):
                    sline = file.readline().rstrip().split('|')
                    bwd_stat[i-1][k, j, 0] = float(sline[0].split('[')[1])
                    bwd_stat[i-1][k, j, 1] = float(sline[1])
                    bwd_stat[i-1][k, j, 2] = float(sline[2])
                    bwd_stat[i-1][k, j, 3] = float(sline[3].split(']')[0])
        return (epochs, batch_size, learning_rate, bits_cut, layers), cost_tpr_time, mlp_stat, fwd_stat, bwd_stat


(epochs, batch_size, learning_rate, bits_cut, layers), cost_tpr_time, mlp_stat, fwd_stat, bwd_stat = parse_mlp('mlp.stat')
print('epochs:', epochs)
print('batch_size:', batch_size)
print('learning_rate:', learning_rate)
print('bits_cut:', bits_cut)
print('layers:', layers)

