#ifndef Q_DATASET_INCLUDE
#define Q_DATASET_INCLUDE

    #include "load_MNIST.h"
    #include "fixed_point.h"


    // get p-th input from dataset
    #define Q_GET_INPUT(set, p) (set->in[set->idcs[p]])
    // get p-th output from dataset
    #define Q_GET_OUTPUT(set, p) (set->out[set->idcs[p]])


    // size: number of examples in dataset
    // idcs: permutation
    // in(inputs), out(outputs)
    typedef struct {
        unsigned int size, *idcs;
        long *a_in, **in;
        long *a_out, **out;
    } q_dataset;


    // allocate MNIST q_dataset from dataset
    q_dataset *alloc_q_dataset_from_dataset(dataset*);
    q_dataset *balanced_subset_q_dataset(q_dataset*, unsigned int);
    void free_q_dataset(q_dataset*);

    // random permute dataset->idcs
    void q_shuffle(q_dataset*);

    void print_q_dataset(q_dataset*);

#endif