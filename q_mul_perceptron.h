#ifndef Q_MUL_PERCEPTRON_INCLUDE
#define Q_MUL_PERCEPTRON_INCLUDE

    #include <math.h>
    #include <time.h>

    #include "utils.h"
    #include "q_dataset.h"
    #include "mul_perceptron.h"
    #include "q_optimizer.h"

/********************************************************************************
    multilayer perceptron: http://neuralnetworksanddeeplearning.com/chap2.html
*********************************************************************************
*********************************************************************************/

    #define MAX_FILENAME_LENGTH 128


    // size: 1 (for input layer) + number of layers
    // layers[i]: number of neurons per layer i
    // weights, biases
    typedef struct q_mlp {
        unsigned int size, *layers;
        long *a_w, **a_wr, ***weights, *a_b, **biases;
    } q_mlp;

    // (z_)activations: temporary hold activations in forward pass
    typedef struct q_t_fwd {
        long *z, *a, **z_activations, **activations;
    } q_t_fwd;

    // weights, biases: temporary hold derivatives in backward pass
    typedef struct q_t_bwd {
        long *a_w, **a_wr, ***weights, *a_b, **biases;
    } q_t_bwd;
    
    extern long sat_min, sat_max;
    extern double fun_tab_low, fun_tab_high;
    extern unsigned int fun_tab_size;

    // allocate and return pointer to q_mlp
    q_mlp *alloc_q_mlp(unsigned int, unsigned int*);
    q_mlp *alloc_q_mlp_from_mlp(mlp*);
    void init_q_mlp_from_q_mlp(q_mlp*, q_mlp*);
    void free_q_mlp(q_mlp*);
    void print_q_mlp(q_mlp*);
    // save mlp to disk
    void save_q_mlp(q_mlp*, char*);
    // allocate, load and return pointer to mlp
    q_mlp *load_alloc_q_mlp(char*);
    
    // allocate t_fwd for forward pass
    q_t_fwd *alloc_q_t_fwd(q_mlp*); // <- TO-DO...
    void free_q_t_fwd(q_t_fwd*);
    void print_q_t_fwd(q_mlp*, q_t_fwd*);
    void print_q_t_fwd_zactivations(q_mlp*, q_t_fwd*);

    // allocate t_bwd for backward pass
    q_t_bwd *alloc_q_t_bwd(q_mlp*);
    void free_q_t_bwd(q_t_bwd*);
    void print_q_t_bwd(q_mlp*, q_t_bwd*);
    // set all derivatives to zero
    void reset_q_t_bwd(q_mlp*, q_t_bwd*);
    // add one derivative to another derivative
    void add_q_t_bwd(q_mlp*, q_t_bwd*, q_t_bwd*);

    // forward pass
    long *q_forward(q_mlp*, q_t_fwd*, q_fun_tab*, long*);
    // backward pass, after function call -> t_bwd contains derivative
    void q_backprop(q_mlp*, q_t_fwd*, q_t_bwd*, q_fun_tab*, q_fun_tab*, long*, long*);

    void q_cost_x(q_mlp*, q_t_fwd*, q_fun_tab*, long*, long*, double*, int*);
    // evaluate mlp's cost function and tpr(true positive rate|accuracy)
    void q_evaluate(q_mlp*, q_t_fwd*, q_fun_tab*, q_dataset*, double*, double*);

    void q_init_for_train(q_mlp*, q_mlp**, q_mlp**, q_t_fwd**, q_t_bwd**, q_t_bwd**, q_fun_tab**, q_fun_tab**);
    void q_free_from_train(q_mlp*, q_mlp*, q_mlp*, q_t_fwd*, q_t_bwd*, q_t_bwd*, q_fun_tab*, q_fun_tab*);
    void q_batch_derivative(q_mlp*, q_t_fwd*, q_t_bwd*, q_t_bwd*, q_fun_tab*, q_fun_tab*, unsigned int, unsigned int, q_dataset*);

    void q_batch_grad_desc(q_mlp*, q_t_fwd*, q_t_bwd*, q_t_bwd*, q_fun_tab*, q_fun_tab*, unsigned int, unsigned int, long, q_dataset*);
    void q_batch_adam(q_mlp*, q_t_fwd*, q_t_bwd*, q_t_bwd*, q_t_bwd*, q_t_bwd*, q_fun_tab*, q_fun_tab*, unsigned int, unsigned int, long, long, long, long, long, q_dataset*);
    
    // train q_mlp: number of epochs is give, remember best performing *weights and *biases
    //              set mlp weights and biases to *weights and *biases
    void q_train_epochs_grad_desc(q_mlp*, unsigned int, unsigned int, long, dataset*);
    void q_train_epochs_adam(q_mlp*, unsigned int, unsigned int, long, long, long, dataset*);

    // train mlp: split train set to train subset and valid set (80%, 20%)
    //            train until cost function grows twice in a row, remember best performing *weights and *biases
    //            set mlp weights and biases to *weights and *biases
    void q_train_grad_desc(q_mlp*, unsigned int, long, dataset*);
    void q_train_adam(q_mlp*, unsigned int, long, long, long, dataset*);

#endif