#include "load_MNIST.h"


labels *load_labels(char *filename) {
    FILE *file = fopen(filename, "rb");
    unsigned char header[8];
    int r;
    r = fread(header, sizeof(unsigned char), 8, file);
    labels *labs = (labels*) malloc(sizeof(labels));
    labs->magic_number = UCHAR_TO_UINT(header);
    labs->size = UCHAR_TO_UINT((&header[4]));
    labs->data = (unsigned char*) malloc(sizeof(unsigned char) * labs->size);
    r = fread(labs->data, sizeof(unsigned char), labs->size, file);
    fclose(file);
    return labs;
}

void free_labels(labels *labs) {
    free(labs->data);
    free(labs);
}

/**********************************************************************************************************************************/

images *load_images(char *filename) {
    FILE *file = fopen(filename, "rb");
    unsigned char header[16];
    int r;
    r = fread(header, sizeof(unsigned char), 16, file);
    images *imgs = (images*) malloc(sizeof(images));
    imgs->magic_number = UCHAR_TO_UINT(header);
    imgs->size = UCHAR_TO_UINT((&header[4]));
    imgs->height = UCHAR_TO_UINT((&header[8]));
    imgs->width = UCHAR_TO_UINT((&header[12]));
    imgs->data = (unsigned char*) malloc(sizeof(unsigned char) * imgs->size * (imgs->height * imgs->width));
    r = fread(imgs->data, sizeof(unsigned char), imgs->size * (imgs->height * imgs->width), file);
    fclose(file);
    return imgs;
}

void free_images(images *imgs) {
    free(imgs->data);
    free(imgs);
}

/**********************************************************************************************************************************/

void save_image(images *imgs, unsigned int p, char *filename) {
    FILE *file = fopen(filename, "wb");
    unsigned char header[64];
    unsigned int header_size = sprintf(header, "P5\n%u %u\n255\n", imgs->width, imgs->height);
    fwrite(header, sizeof(char), header_size, file);
    fwrite(GET_IMAGE(imgs, p), sizeof(char), imgs->height * imgs->width, file);
    fclose(file);
}

/**********************************************************************************************************************************/

void test_MNIST(int argc, char *argv[]) {
    char filename[30];
    labels *labs = load_labels("mnist_unzipped/train-labels.idx1-ubyte");
    images *imgs = load_images("mnist_unzipped/train-images.idx3-ubyte");
    int i;
    for(i = 1; i < argc; i++) {
        int p = atoi(argv[i]);
        sprintf(filename, "test_%u_%u.pgm", p, labs->data[p]);
        save_image(imgs, p, filename);
    }
    free_images(imgs);
    free_labels(labs);
}
